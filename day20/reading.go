package main

import (
	"bufio"
	"fmt"
	"io"
	"strconv"
)

func read_tiles(r io.Reader) ([][]string, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	var out [][]string
	out = append(out, make([]string, 0))
	i := 0
	for scanner.Scan() {
		s_map := scanner.Text()
		if s_map == "" {
			out = append(out, make([]string, 0))
			i++
			continue
		}
		out[i] = append(out[i], s_map)
	}
	return out, scanner.Err()
}

func AtoUI(str string) uint {
	num, err := strconv.Atoi(str)
	if err != nil {
		fmt.Println(err)
	}
	if num < 0 {
		fmt.Println("tile ID < 0!? = ", num)
	}
	return uint(num)
}

func hashdot_to_bool(str string) []bool {
	out := make([]bool, len(str))
	for i, r := range str {
		switch r {
		case '#':
			out[i] = true
		case '.':
			out[i] = false
		case ' ':
			out[i] = false
		}
	}
	return out
}

func strings_to_tiles(vs_tiles [][]string) []tile {
	out := make([]tile, len(vs_tiles))
	for i, s_tile := range vs_tiles {
		if len(s_tile) != 0 {
			out[i] = make_tile(s_tile)
		}
	}
	return out
}
