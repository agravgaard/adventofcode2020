package main

import (
	"fmt"
	"strings"
)

type tile struct {
	id   uint
	tile [][]bool
}

func make_tile(s_tile []string) tile {
	out := tile{}
	out.tile = make([][]bool, len(s_tile)-1)
	for i, str := range s_tile {
		if i == 0 {
			id_str := strings.Split(str, " ")[1]
			out.id = AtoUI(strings.TrimRight(id_str, ":"))
		} else {
			out.tile[i-1] = hashdot_to_bool(str)
		}
	}
	return out
}

func copy_tile(in_tile tile) tile {
	out := tile{}
	out.id = in_tile.id
	out.tile = make([][]bool, len(in_tile.tile))
	for i := 0; i < len(out.tile); i++ {
		out.tile[i] = make([]bool, len(in_tile.tile[0]))
		copy(out.tile[i], in_tile.tile[i])
	}
	return out
}

func is_same(a, b []bool) bool {
	if len(a) != len(b) {
		return false
	}
	for i, a_i := range a {
		if a_i != b[i] {
			return false
		}
	}
	return true
}

func flip_bslice(a []bool) []bool {
	b := make([]bool, len(a))
	copy(b, a)
	for left, right := 0, len(b)-1; left < right; left, right = left+1, right-1 {
		b[left], b[right] = b[right], b[left]
	}
	return b
}
func flip_bsliceslice(a [][]bool) [][]bool {
	b := make([][]bool, len(a))
	for i, arow := range a {
		b[i] = make([]bool, len(arow))
		copy(b[i], arow)
	}
	for left, right := 0, len(b)-1; left < right; left, right = left+1, right-1 {
		b[left], b[right] = b[right], b[left]
	}
	return b
}

func flipy(a tile) tile {
	out := tile{}
	out.id = a.id
	out.tile = flip_bsliceslice(a.tile)
	return out
}
func flipx(a tile) tile {
	out := tile{}
	out.id = a.id
	out.tile = make([][]bool, len(a.tile))
	for i, row_a := range a.tile {
		out.tile[i] = flip_bslice(row_a)
	}
	return out
}

func bcol(a [][]bool, index int) []bool {
	out := make([]bool, len(a))
	for i, row := range a {
		out[i] = row[index]
	}
	return out
}
func rotr(a tile) tile {
	out := tile{}
	out.id = a.id
	out.tile = make([][]bool, len(a.tile[0]))
	for i := range a.tile[0] {
		out.tile[i] = flip_bslice(bcol(a.tile, i))
	}
	return out
}

func get_r(a tile) []bool {
	return bcol(a.tile, len(a.tile[0])-1)
}
func get_l(a tile) []bool {
	return bcol(a.tile, 0)
}
func get_t(a tile) []bool {
	return a.tile[0]
}
func get_b(a tile) []bool {
	return a.tile[len(a.tile)-1]
}

func match_flip_turn(face []bool, tl_b tile) (bool, tile) {
	for n_ft := 0; n_ft < 8; n_ft++ {
		tl_ft := get_flip_x_y_turn(n_ft, tl_b)
		if is_same(face, get_l(tl_ft)) {
			return true, tl_ft
		}
	}
	return false, tile{}
}

func find_match_face(face []bool, self_id uint, tiles []tile) (bool, tile) {
	found := false
	var out tile
	for _, tl_b := range tiles {
		if tl_b.id == self_id {
			continue
		}
		found, out = match_flip_turn(face, tl_b)
		if found {
			return true, out
		}
	}
	return false, tile{}
}

func find_match_right(tl_a tile, tiles []tile) (bool, tile) {
	face := get_r(tl_a)
	found, tl_out := find_match_face(face, tl_a.id, tiles)
	return found, tl_out
}
func find_match_bottom(tl_a tile, tiles []tile) bool {
	face := get_b(tl_a)
	found, _ := find_match_face(face, tl_a.id, tiles)
	return found
}
func find_match_left(tl_a tile, tiles []tile) bool {
	face := get_l(tl_a)
	found, _ := find_match_face(face, tl_a.id, tiles)
	return found
}
func find_match_top(tl_a tile, tiles []tile) bool {
	face := get_t(tl_a)
	found, _ := find_match_face(face, tl_a.id, tiles)
	return found
}

func get_flip_x_y_turn(n int, a tile) tile {
	switch n {
	case 0:
		return a
	case 1:
		return flipx(a)
	case 2:
		return flipy(a)
	case 3:
		return flipx(flipy(a))
	case 4:
		return rotr(a)
	case 5:
		return flipx(rotr(a))
	case 6:
		return flipy(rotr(a))
	case 7:
		return flipx(flipy(rotr(a)))
	}
	return a
}
func invert_flip_x_y_turn(n int, a tile) tile {
	switch n {
	case 0:
		return a
	case 1:
		return flipx(a)
	case 2:
		return flipy(a)
	case 3:
		return flipx(flipy(a))
	case 4:
		return rotr(rotr(rotr(a)))
	case 5:
		return rotr(rotr(rotr(flipx(a))))
	case 6:
		return rotr(rotr(rotr(flipy(a))))
	case 7:
		return rotr(rotr(rotr(flipx(flipy(a)))))
	}
	return a
}

func is_topleft_corner(a tile, tiles []tile) bool {
	found_r, _ := find_match_right(a, tiles)
	if !found_r {
		// must have a right match
		return false
	}
	found_b := find_match_bottom(a, tiles)
	if !found_b {
		// must have a bottom match
		return false
	}
	found_l := find_match_left(a, tiles)
	if found_l {
		// must NOT have a left match
		return false
	}
	found_t := find_match_top(a, tiles)
	// must NOT have a top match
	return !found_t
}

func remove_by_id(id uint, tiles *[]tile) bool {
	idx_to_rm := -1
	for i, tl := range *tiles {
		if tl.id == id {
			idx_to_rm = i
			break
		}
	}
	if idx_to_rm == -1 {
		return false
	}
	*tiles = append((*tiles)[:idx_to_rm], (*tiles)[idx_to_rm+1:]...)
	return true
}

func long_to_2D(in []tile, width int) [][]tile {
	height := 12
	out := make([][]tile, height)

	i := 0
	for x := 0; x < height; x++ {
		out[x] = make([]tile, width)
		for y := 0; y < width; y++ {
			out[x][y] = in[i]
			i++
		}
	}
	return out
}

func build_row(start tile, tiles *[]tile) []tile {
	var out []tile
	out = append(out, start)
	remove_by_id(start.id, tiles)
	for i := 0; ; i++ {
		if len(*tiles) == 0 {
			break
		}
		found, tl_r := find_match_right(out[i], *tiles)
		if !found {
			break
		}
		out = append(out, tl_r)
		remove_by_id(tl_r.id, tiles)
	}
	return out
}

func find_topleft_corner(above tile, tiles []tile) (bool, tile) {
	for _, tl := range tiles {
		for n_ft := 0; n_ft < 8; n_ft++ {
			tl_ft := get_flip_x_y_turn(n_ft, tl)
			if is_topleft_corner(tl_ft, tiles) {
				if above.id == 0 {
					return true, tl_ft
				}
				if is_same(get_b(above), get_t(tl_ft)) {
					return true, tl_ft
				}
			}
		}
	}
	return false, tile{}
}

func match_tiles(tiles []tile) [][]tile {
	width := len(tiles)
	height := len(tiles)
	var out_long []tile
	corner := tile{}
	for i := 0; i < len(tiles); i++ {
		if len(tiles) == 0 {
			break
		}
		found := false
		if i == height-1 {
			//last row
			top_match := get_b(corner)
			found, corner = find_match_face(top_match, corner.id, tiles)
			corner = flipx(rotr(corner))
		} else {
			found, corner = find_topleft_corner(corner, tiles)
		}
		if !found {
			fmt.Println("Didn't find corner for row", i)
			break
		}
		row := build_row(corner, &tiles)
		if len(row) != width {
			width = len(row)
			height = (len(tiles) / width) + 1
			fmt.Println("w:", width, "h:", height)
		}
		out_long = append(out_long, row...)
	}
	return long_to_2D(out_long, width)
}

func remove_border(tl tile) tile {
	out := tile{}
	out.id = tl.id
	nrows := len(tl.tile) - 2
	row_len := len(tl.tile[0]) - 2
	out.tile = make([][]bool, nrows)
	for i := 1; i < (nrows + 1); i++ {
		out.tile[i-1] = make([]bool, row_len)
		copy(out.tile[i-1], tl.tile[i][1:row_len+1])
	}
	return out
}

func remove_borders(pic *[][]tile) {
	for x, row := range *pic {
		for y, tl := range row {
			(*pic)[x][y] = remove_border(tl)
		}
	}
}

func merge(picture [][]tile) tile {
	megatile := tile{}
	megatile.id = 69
	tl_rows := len(picture[0][0].tile)
	tl_cols := len(picture[0][0].tile[0])
	fmt.Println("tl h:", tl_cols, "w:", tl_rows)
	nrows := len(picture) * tl_rows
	ncols := len(picture[0]) * tl_cols
	megatile.tile = make([][]bool, nrows)
	for i, row := range picture {
		for x := 0; x < tl_rows; x++ {
			x_idx := i*tl_rows + x
			megatile.tile[x_idx] = make([]bool, ncols)
			for j, tl := range row {
				for y := 0; y < tl_cols; y++ {
					y_idx := j*tl_cols + y
					megatile.tile[x_idx][y_idx] = tl.tile[x][y]
				}
			}
		}
	}
	return megatile
}

func match_trues(t, b []bool) bool {
	if len(t) != len(b) {
		return false
	}
	for i, a_i := range t {
		if a_i && !b[i] {
			return false
		}
	}
	return true
}

func rm_trues_at(trues [][]bool, i, j int, tl *tile) {
	nrow := len(trues)
	ncol := len(trues[0])
	for x, xp := i, 0; x < i+nrow; x, xp = x+1, xp+1 {
		for y, yp := j, 0; y < j+ncol; y, yp = y+1, yp+1 {
			if trues[xp][yp] {
				tl.tile[x][y] = false
			}
		}
	}
}

func remove_seamonsters(megatile *tile, b_sea_monster [][]bool) {
	pattern_bot := b_sea_monster[2]
	pattern_mid := b_sea_monster[1]
	pattern_top := b_sea_monster[0]
	p_len := len(pattern_bot)
	for i, row_bot := range megatile.tile {
		if i == 0 || i == 1 {
			continue
		}
                row_top := megatile.tile[i-2]
                row_mid := megatile.tile[i-1]
		for j := range row_bot {
			if j+p_len > len(row_bot) {
				break
			}
			if match_trues(pattern_bot, row_bot[j:j+p_len]) {
				if match_trues(pattern_mid, row_mid[j:j+p_len]) {
					if match_trues(pattern_top, row_top[j:j+p_len]) {
						rm_trues_at(b_sea_monster, i-2, j, megatile)
					}
				}
			}
		}
	}
}

func b_and(a_inout *tile, b_in tile) {
	for i, row_b := range b_in.tile {
		for j, b := range row_b {
			a_inout.tile[i][j] = a_inout.tile[i][j] && b
		}
	}
}
func remove_seamonsters_ft(megatile tile, b_sea_monster [][]bool) tile {
	out := copy_tile(megatile)
	for n_ft := 0; n_ft < 8; n_ft++ {
		tl_ft := get_flip_x_y_turn(n_ft, megatile)
		remove_seamonsters(&tl_ft, b_sea_monster)
		tl_invft := invert_flip_x_y_turn(n_ft, tl_ft)
		b_and(&out, tl_invft)
	}
	return out
}

func count_true(megatile tile) uint {
	count := uint(0)
	for _, row := range megatile.tile {
		for _, b := range row {
			if b {
				count++
			}
		}
	}
	return count
}
