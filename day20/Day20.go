package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	fp, err := os.Open("input_Day20.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	vs_tiles, err := read_tiles(reader)
	if err != nil {
		fmt.Println(err)
		return
	}

	tiles := strings_to_tiles(vs_tiles)
	for _, tl := range tiles {
		if tl.id == 0 {
			remove_by_id(0, &tiles)
		}
	}

	picture := match_tiles(tiles)
	mult := picture[0][0].id
	mult *= picture[len(picture)-1][0].id
	mult *= picture[0][len(picture[0])-1].id
	mult *= picture[len(picture)-1][len(picture[0])-1].id
	fmt.Println("Mult corners =", mult)

	// remove borders
	remove_borders(&picture)
	// merge into one big tile
	megatile := merge(picture)

	// something about sea monsters
	// Sea monster pattern:
	s_sea_monster := []string{
		"                  # ",
		"#    ##    ##    ###",
		" #  #  #  #  #  #   ",
	}
	b_sea_monster := make([][]bool, 3)
	b_sea_monster[0] = hashdot_to_bool(s_sea_monster[0])
	b_sea_monster[1] = hashdot_to_bool(s_sea_monster[1])
	b_sea_monster[2] = hashdot_to_bool(s_sea_monster[2])

	megatile = remove_seamonsters_ft(megatile, b_sea_monster)
	count := count_true(megatile)
	fmt.Println("non-seamonster count:", count)

}
