package main

import (
	"fmt"
	"io"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	// "github.com/stretchr/testify/mock"
	// "github.com/stretchr/testify/require"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func TestReader(t *testing.T) {
	fp, err := os.Open("input_test.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	vs_tiles, err := read_tiles(reader)
	if err != nil {
		fmt.Println(err)
		return
	}

	true_ids := []uint{
		2311,
		1951,
		1171,
		1427,
		1489,
		2473,
		2971,
		2729,
		3079,
	}

	assert.Equal(t, len(true_ids), len(vs_tiles))

	tiles := strings_to_tiles(vs_tiles)
	assert.Equal(t, len(true_ids), len(tiles))

	for i, id := range true_ids {
		assert.Equal(t, id, tiles[i].id)
	}
}

func make_test_tile() tile {
	out := tile{}
	out.id = 69
	out.tile = [][]bool{
		{true, true, false},
		{false, true, false},
		{false, false, false},
	}
	return out
}

func TestRotate(t *testing.T) {
	test_tl := make_test_tile()
	rot_tl := rotr(test_tl)

	// Rotation should not alter input:
	test_tl_2 := make_test_tile()
	assert.Equal(t, test_tl, test_tl_2)

	// Test rotation worked as expexted:
	true_rot := [][]bool{
		{false, false, true},
		{false, true, true},
		{false, false, false},
	}
	assert.Equal(t, true_rot, rot_tl.tile)
}
func TestFlipX(t *testing.T) {
	test_tl := make_test_tile()
	flpx_tl := flipx(test_tl)

	// flip x should not alter input:
	test_tl_2 := make_test_tile()
	assert.Equal(t, test_tl, test_tl_2)

	// Test flip x worked as expexted:
	true_flpx := [][]bool{
		{false, true, true},
		{false, true, false},
		{false, false, false},
	}
	assert.Equal(t, true_flpx, flpx_tl.tile)
}
func TestFlipY(t *testing.T) {
	test_tl := make_test_tile()
	flpy_tl := flipy(test_tl)

	// flip y should not alter input:
	test_tl_2 := make_test_tile()
	assert.Equal(t, test_tl, test_tl_2)

	// Test flip y worked as expexted:
	true_flpy := [][]bool{
		{false, false, false},
		{false, true, false},
		{true, true, false},
	}
	assert.Equal(t, true_flpy, flpy_tl.tile)
}
func TestRemoveBorder(t *testing.T) {
	test_tl := make_test_tile()
	rmbd_tl := remove_border(test_tl)

	// flpxation should not alter input:
	test_tl_2 := make_test_tile()
	assert.Equal(t, test_tl, test_tl_2)

	// Test removal worked as expexted:
	true_rmbd := [][]bool{
		{true},
	}
	assert.Equal(t, true_rmbd, rmbd_tl.tile)
}

func TestMatchTrues(t *testing.T) {
	test_tl := make_test_tile()
	s_pattern := []string{
		"# ",
		" #",
		"  ",
	}
	b_pattern := make([][]bool, 3)
	b_pattern[0] = hashdot_to_bool(s_pattern[0])
	b_pattern[1] = hashdot_to_bool(s_pattern[1])
	b_pattern[2] = hashdot_to_bool(s_pattern[2])

	assert.True(t, match_trues(b_pattern[0], test_tl.tile[0][0:2]))
	assert.True(t, match_trues(b_pattern[0], test_tl.tile[0][1:3]))
	assert.True(t, match_trues(b_pattern[1], test_tl.tile[1][0:2]))
	assert.False(t, match_trues(b_pattern[1], test_tl.tile[1][1:3]))
	assert.True(t, match_trues(b_pattern[2], test_tl.tile[2][0:2]))
	assert.True(t, match_trues(b_pattern[2], test_tl.tile[2][1:3]))

}
func TestRemoveSeamonster(t *testing.T) {
	test_tl := make_test_tile()
	s_pattern := []string{
		"# ",
		" #",
		"  ",
	}
	b_pattern := make([][]bool, 3)
	b_pattern[0] = hashdot_to_bool(s_pattern[0])
	b_pattern[1] = hashdot_to_bool(s_pattern[1])
	b_pattern[2] = hashdot_to_bool(s_pattern[2])

	remove_seamonsters(&test_tl, b_pattern)

	// Test removal worked as expexted:
	true_rmsm := [][]bool{
		{false, true, false},
		{false, false, false},
		{false, false, false},
	}
	assert.Equal(t, true_rmsm, test_tl.tile)
}
func TestRemoveSeamonster2(t *testing.T) {
	test_tl := make_test_tile()
	test_tl.tile = [][]bool{
		{false, true, false},
		{false, false, true},
		{false, true, false},
	}
	s_pattern := []string{
		"# ",
		" #",
		"  ",
	}
	b_pattern := make([][]bool, 3)
	b_pattern[0] = hashdot_to_bool(s_pattern[0])
	b_pattern[1] = hashdot_to_bool(s_pattern[1])
	b_pattern[2] = hashdot_to_bool(s_pattern[2])

	remove_seamonsters(&test_tl, b_pattern)

	// Test removal worked as expexted:
	true_rmsm := [][]bool{
		{false, false, false},
		{false, false, false},
		{false, true, false},
	}
	assert.Equal(t, true_rmsm, test_tl.tile)
}
func TestRemoveSeamonsterFlipTurn(t *testing.T) {
	test_tl := tile{}
	test_tl.id = 69
	test_tl.tile = [][]bool{
		{false, true, true, false, true},
		{false, false, true, true, true},
		{false, false, false, false, false},
		{false, true, false, true, false},
		{true, true, false, true, true},
	}
	s_pattern := []string{
		"##",
		" #",
		"  ",
	}
	b_pattern := make([][]bool, 3)
	b_pattern[0] = hashdot_to_bool(s_pattern[0])
	b_pattern[1] = hashdot_to_bool(s_pattern[1])
	b_pattern[2] = hashdot_to_bool(s_pattern[2])

	test_tl = remove_seamonsters_ft(test_tl, b_pattern)

	// Test removal worked as expexted:
	true_rmsm := [][]bool{
		{false, false, false, false, false},
		{false, false, false, false, false},
		{false, false, false, false, false},
		{false, false, false, false, false},
		{false, false, false, false, false},
	}
	assert.Equal(t, true_rmsm, test_tl.tile)
}
func TestCountTrue(t *testing.T) {
	test_tl := make_test_tile()
	count := count_true(test_tl)
	assert.Equal(t, uint(3), count)
}

func TestFlipXYTurns0(t *testing.T) {
	test_tl := make_test_tile()
	ft_tl := get_flip_x_y_turn(0, test_tl)

	// Should not alter input:
	test_tl_2 := make_test_tile()
	assert.Equal(t, test_tl, test_tl_2)

	// Test it worked as expexted:
	assert.Equal(t, ft_tl.tile, test_tl_2.tile)
}
func TestFlipXYTurns1(t *testing.T) {
	test_tl := make_test_tile()
	ft_tl := get_flip_x_y_turn(1, test_tl)

	// Should not alter input:
	test_tl_2 := make_test_tile()
	assert.Equal(t, test_tl, test_tl_2)

	// Test it worked as expexted:
	test_tl_fx := flipx(test_tl_2)
	assert.Equal(t, ft_tl.tile, test_tl_fx.tile)
}
func TestFlipXYTurns2(t *testing.T) {
	test_tl := make_test_tile()
	ft_tl := get_flip_x_y_turn(2, test_tl)

	// Should not alter input:
	test_tl_2 := make_test_tile()
	assert.Equal(t, test_tl, test_tl_2)

	// Test it worked as expexted:
	test_tl_fy := flipy(test_tl_2)
	assert.Equal(t, ft_tl.tile, test_tl_fy.tile)
}
func TestInvertFlipXYTurns(t *testing.T) {
	test_tl := make_test_tile()
	for n_ft := 0; n_ft < 8; n_ft++ {
		ft_tl := get_flip_x_y_turn(n_ft, test_tl)
		inv_ft_tl := invert_flip_x_y_turn(n_ft, ft_tl)
		assert.Equal(t, test_tl, inv_ft_tl)
	}
}
