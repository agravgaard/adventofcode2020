package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
)

func combinations_that_sum_to(vec []int, sum_to int) [][2]int {
	sort.Ints(vec)
	var vv_out [][2]int
	inv_i := (len(vec) - 1)
	for i, val := range vec {
		for ; inv_i > i; inv_i-- {
			sum := val + vec[inv_i]
			if sum == sum_to {
				vv_out = append(vv_out, [2]int{val, vec[inv_i]})
			} else if sum < sum_to {
				break
			}
		}
	}
	return vv_out
}

func ReadInts(r io.Reader) ([]int, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)
	var result []int
	for scanner.Scan() {
		x, err := strconv.Atoi(scanner.Text())
		if err != nil {
			return result, err
		}
		result = append(result, x)
	}
	return result, scanner.Err()
}

func main_p1() {
	fp, err := os.Open("input.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	vec, err := ReadInts(reader)
	if err != nil {
		fmt.Println(err)
		return
	}

	vec_out := combinations_that_sum_to(vec, 2020)
	for _, v_pair := range vec_out {
		fmt.Printf("%d * %d = %d\n",
			v_pair[0], v_pair[1],
			v_pair[0]*v_pair[1])
	}
}
