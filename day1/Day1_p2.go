package main

import (
	"fmt"
	"io"
	"os"
	"sort"
)

func triple_combinations_that_sum_to(vec []int, sum_to int) [][3]int {
	sort.Ints(vec)
	var vv_out [][3]int
	for i_o, val_outer := range vec {
		inv_i := (len(vec) - 1)
		for i_i := (i_o + 1); i_i < len(vec); i_i++ {
			val_inner := vec[i_i]
			for ; inv_i > i_i; inv_i-- {
				sum := val_inner + val_outer + vec[inv_i]
				if sum == sum_to {
					vv_out = append(vv_out, [3]int{val_inner, val_outer, vec[inv_i]})
				} else if sum < sum_to {
					break
				}
			}
		}
	}
	return vv_out
}

func main() {
        main_p1()

	fp, err := os.Open("input.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	vec, err := ReadInts(reader)
	if err != nil {
		fmt.Println(err)
		return
	}

	vec_out := triple_combinations_that_sum_to(vec, 2020)
	for _, v_pair := range vec_out {
		fmt.Printf("%d * %d * %d = %d\n",
			v_pair[0], v_pair[1], v_pair[2],
			v_pair[0]*v_pair[1]*v_pair[2])
	}
}
