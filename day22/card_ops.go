package main

import (
	"strconv"
	"strings"
)

func play_recursive_round(cards [2][]uint) [2][]uint {
	p0 := cards[0][0]
	p1 := cards[1][0]

	l_p0_cards := uint(len(cards[0]))
	l_p1_cards := uint(len(cards[1]))
	p0_win := false
	p1_win := false
	if p0+1 <= l_p0_cards && p1+1 <= l_p1_cards {
		var sub_cards [2][]uint
		sub_cards[0] = make([]uint, p0)
		sub_cards[1] = make([]uint, p1)
		copy(sub_cards[0], cards[0][1:p0+1])
		copy(sub_cards[1], cards[1][1:p1+1])
		p1_win, _ = play_recursive(sub_cards)
		p0_win = !p1_win
	}

	if p1_win || (p1 > p0 && !p0_win) {
		cards[1] = append(cards[1][1:], p1, p0)
		cards[0] = cards[0][1:]
	} else {
		cards[0] = append(cards[0][1:], p0, p1)
		cards[1] = cards[1][1:]
	}
	return cards
}

func make_signature(cards [2][]uint) string {
	sig_str_0 := make([]string, len(cards[0]))
	for _, i := range cards[0] {
		sig_str_0 = append(sig_str_0, strconv.Itoa(int(i)))
	}
	sig_str_1 := make([]string, len(cards[1]))
	for _, i := range cards[1] {
		sig_str_1 = append(sig_str_1, strconv.Itoa(int(i)))
	}
	return strings.Join(sig_str_0, ",") + "|" + strings.Join(sig_str_1, ",")
}

func play_recursive(cards [2][]uint) (bool, []uint) {
	signatures := make(map[string]bool)
	for {
		if len(cards[0]) == 0 || len(cards[1]) == 0 {
			// Game over when one player has no more cards
			break
		}
		new_sig := make_signature(cards)
		if _, ok := signatures[new_sig]; ok {
			// Infinite game, Player 0 wins the game
			break
		} else {
			signatures[new_sig] = true
		}
		cards = play_recursive_round(cards)
	}

	// Return the winners cards
	if len(cards[0]) == 0 {
		return true, cards[1]
	}
	return false, cards[0]
}
func play_round(cards [2][]uint) [2][]uint {
	p0 := cards[0][0]
	p1 := cards[1][0]
	if p1 > p0 {
		cards[1] = append(cards[1][1:], p1, p0)
		cards[0] = cards[0][1:]
	} else {
		cards[0] = append(cards[0][1:], p0, p1)
		cards[1] = cards[1][1:]
	}
	return cards
}

func play(cards [2][]uint) []uint {
	for {
		if len(cards[0]) == 0 || len(cards[1]) == 0 {
			// Game over when one player has no more cards
			break
		}
		cards = play_round(cards)
	}

	// Return the winners cards
	if len(cards[0]) == 0 {
		return cards[1]
	}
	return cards[0]
}

func calculate_points(cards []uint) uint {
	points := uint(0)
	l_cards := uint(len(cards))
	for i, c := range cards {
		points += (l_cards - uint(i)) * c
	}
	return points
}
