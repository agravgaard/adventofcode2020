package main

import (
	"io"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	// "github.com/stretchr/testify/mock"
	// "github.com/stretchr/testify/require"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func make_test_cards() [2][]uint {
	return [2][]uint{
		{9, 2, 6, 3, 1},
		{5, 8, 4, 7, 10},
	}
}
func make_test_winner_cards() []uint {
	return []uint{3, 2, 10, 6, 8, 5, 9, 4, 7, 1}
}
func make_test_winner_cards_recursive() []uint {
	return []uint{7, 5, 6, 2, 4, 1, 10, 8, 9, 3}
}

func TestReader(t *testing.T) {
	fp, err := os.Open("input_test.txt")
	assert.NoError(t, err)
	reader := io.Reader(fp)
	cards, err := read_cards(reader)
	assert.NoError(t, err)

	true_cards := make_test_cards()

	assert.Equal(t, true_cards, cards)
}

func TestPlay(t *testing.T) {
	test_cards := make_test_cards()
	test_winner_cards := play(test_cards)
	// Make sure playing doesn't change cards:
	assert.Equal(t, make_test_cards(), test_cards)

	true_winner_cards := make_test_winner_cards()
	assert.Equal(t, true_winner_cards, test_winner_cards)
}

func TestPlayRecursive(t *testing.T) {
	test_cards := make_test_cards()
	_, test_winner_cards := play_recursive(test_cards)
	true_winner_cards := make_test_winner_cards_recursive()
	assert.Equal(t, true_winner_cards, test_winner_cards)
}

func TestCountPoints(t *testing.T) {
	test_winner_cards := make_test_winner_cards()
	test_points := calculate_points(test_winner_cards)
	assert.Equal(t, uint(306), test_points)
}
