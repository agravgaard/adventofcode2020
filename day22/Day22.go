package main

import (
	"fmt"
)

func part1() {
	cards := read_file("input_Day22.txt")
	cards_winner := play(cards)
	points := calculate_points(cards_winner)
	fmt.Println("Winners points:", points)
}
func part2() {
	cards_rec := read_file("input_Day22.txt")
	_, cards_winner_rec := play_recursive(cards_rec)
	points_rec := calculate_points(cards_winner_rec)
	fmt.Println("Recursive winners points:", points_rec)
}

func main() {
	part1()
	part2()
}
