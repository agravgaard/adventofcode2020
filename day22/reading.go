package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func read_file(filename string) [2][]uint {
	fp, err := os.Open(filename)
	if err != nil {
		fmt.Println(err)
	}
	reader := io.Reader(fp)
	cards, err := read_cards(reader)
	if err != nil {
		fmt.Println(err)
	}
	return cards
}

func a_to_uint(str string) uint {
	val, err := strconv.Atoi(str)
	if err != nil {
		fmt.Println(err)
	}
	return uint(val)
}

func read_cards(r io.Reader) ([2][]uint, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	var out [2][]uint
	player := 0
	for scanner.Scan() {
		s_map := scanner.Text()
		if strings.HasPrefix(s_map, "Player") {
			continue
		}
		if s_map == "" {
			player++
			continue
		}
		out[player] = append(out[player], a_to_uint(s_map))
	}
	return out, scanner.Err()
}
