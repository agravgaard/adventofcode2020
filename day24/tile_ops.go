package main

import (
	"strconv"
	"strings"
)

func add_vectors(vectors [][2]int) [2]int {
	out := [2]int{0, 0}
	for _, v := range vectors {
		out[0] += v[0]
		out[1] += v[1]
	}
	return out
}

func vec_to_str(vec [2]int) string {
	x := strconv.Itoa(vec[0])
	y := strconv.Itoa(vec[1])
	return strings.Join([]string{x, y}, ",")
}
func xystr_to_vec(str string) [2]int {
	out := [2]int{0, 0}
	x_y := strings.Split(str, ",")
	out[0], _ = strconv.Atoi(x_y[0])
	out[1], _ = strconv.Atoi(x_y[1])
	return out
}

func vectors_to_map(vectors [][2]int) map[string]bool {
	out_map := make(map[string]bool)
	for _, v := range vectors {
		str := vec_to_str(v)
		if val, ok := out_map[str]; ok {
			if val {
				out_map[str] = false
			} else {
				out_map[str] = true
			}
		} else {
			out_map[str] = true
		}
	}
	return out_map
}

func lines_to_vectorsums(lines []string) [][2]int {
	out := make([][2]int, len(lines))
	for i, line := range lines {
		vec := line_to_vectors(line)
		out[i] = add_vectors(vec)
	}
	return out
}

func count_true_map(input map[string]bool) int {
	count := 0
	for _, val := range input {
		if val {
			count++
		}
	}
	return count
}

func add_and_stringify(a, b [2]int) string {
	c := [2]int{0, 0}
	c[0] = a[0] + b[0]
	c[1] = a[1] + b[1]
	return vec_to_str(c)
}

func count_true_neighbours(centre string, in_map map[string]bool) uint {
	count := uint(0)
	c_vec := xystr_to_vec(centre)

	ne := compass_to_vector("ne")
	e := compass_to_vector("e")
	se := compass_to_vector("se")
	sw := compass_to_vector("sw")
	w := compass_to_vector("w")
	nw := compass_to_vector("nw")
	if val, ok := in_map[add_and_stringify(c_vec, ne)]; ok {
		if val {
			count++
		}
	}

	if val, ok := in_map[add_and_stringify(c_vec, e)]; ok {
		if val {
			count++
		}
	}
	if val, ok := in_map[add_and_stringify(c_vec, se)]; ok {
		if val {
			count++
		}
	}
	if val, ok := in_map[add_and_stringify(c_vec, sw)]; ok {
		if val {
			count++
		}
	}
	if val, ok := in_map[add_and_stringify(c_vec, w)]; ok {
		if val {
			count++
		}
	}
	if val, ok := in_map[add_and_stringify(c_vec, nw)]; ok {
		if val {
			count++
		}
	}

	return count

}
func add_false_neighbours(centre string, in_map map[string]bool) map[string]bool {
	c_vec := xystr_to_vec(centre)

	ne := compass_to_vector("ne")
	e := compass_to_vector("e")
	se := compass_to_vector("se")
	sw := compass_to_vector("sw")
	w := compass_to_vector("w")
	nw := compass_to_vector("nw")

	if _, ok := in_map[add_and_stringify(c_vec, ne)]; !ok {
		in_map[add_and_stringify(c_vec, ne)] = false
	}
	if _, ok := in_map[add_and_stringify(c_vec, e)]; !ok {
		in_map[add_and_stringify(c_vec, e)] = false
	}
	if _, ok := in_map[add_and_stringify(c_vec, se)]; !ok {
		in_map[add_and_stringify(c_vec, se)] = false
	}
	if _, ok := in_map[add_and_stringify(c_vec, sw)]; !ok {
		in_map[add_and_stringify(c_vec, sw)] = false
	}
	if _, ok := in_map[add_and_stringify(c_vec, w)]; !ok {
		in_map[add_and_stringify(c_vec, w)] = false
	}
	if _, ok := in_map[add_and_stringify(c_vec, nw)]; !ok {
		in_map[add_and_stringify(c_vec, nw)] = false
	}

	return in_map
}
func add_false_around_trues(in_map map[string]bool) map[string]bool {
	for key, val := range in_map {
		if val {
			in_map = add_false_neighbours(key, in_map)
		}
	}
	return in_map
}

func keys_to_flip(in_map map[string]bool) []string {
	var out []string
	for key, val := range in_map {
		count_nb := count_true_neighbours(key, in_map)
		if val && (count_nb == 0 || count_nb > 2) {
			out = append(out, key)
		} else if !val && count_nb == 2 {
			out = append(out, key)
		}
	}
	return out
}
func flip_keys(ktf []string, in_map map[string]bool) map[string]bool {
	for _, key := range ktf {
		if in_map[key] {
			in_map[key] = false
		} else {
			in_map[key] = true
		}
	}
	return in_map
}

func flip_days(in_map map[string]bool, n_days uint) map[string]bool {
	for day := uint(0); day < n_days; day++ {
		in_map = add_false_around_trues(in_map)
		ktf := keys_to_flip(in_map)
		in_map = flip_keys(ktf, in_map)
	}
	return in_map
}
