package main

import (
	"bufio"
	"log"
	"io"
	"os"
)

func read_file(filename string) []string {
	fp, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	reader := io.Reader(fp)
	cards, err := read_lines(reader)
	if err != nil {
		log.Fatal(err)
	}
	return cards
}

func compass_to_vector(str string) [2]int {
        out := [2]int{0, 0}
        switch str {
        case "ne":
            out[0] = 5
            out[1] = 10
        case "e":
            out[0] = 10
        case "se":
            out[0] = 5
            out[1] = -10
        case "sw":
            out[0] = -5
            out[1] = -10
        case "w":
            out[0] = -10
        case "nw":
            out[0] = -5
            out[1] = 10

        }
	return out
}

func line_to_vectors(line string) [][2]int {
    var out [][2]int
    for i := 0; i < len(line); i++ {
        str := string(line[i])
        if str == "s" || str == "n" {
            i++
            str += string(line[i])
        }
        out = append(out, compass_to_vector(str))
    }
    return out
}

func read_lines(r io.Reader) ([]string, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	var out []string
	for scanner.Scan() {
		s_map := scanner.Text()
		out = append(out, s_map)
	}
	return out, scanner.Err()
}
