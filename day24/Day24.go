package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime/pprof"
)

func part_1() {
	lines := read_file("input_Day24.txt")
	vecs := lines_to_vectorsums(lines)
	vecmap := vectors_to_map(vecs)
	count := count_true_map(vecmap)
	fmt.Println("Part 1, count:", count)
}
func part_2() {
	lines := read_file("input_Day24.txt")
	vecs := lines_to_vectorsums(lines)
	vecmap := vectors_to_map(vecs)
	vecmap = flip_days(vecmap, 100)
	count := count_true_map(vecmap)
	fmt.Println("Part 2, count:", count)
}

var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")

func main() {
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		err = pprof.StartCPUProfile(f)
		if err != nil {
			log.Fatal(err)
		}
		defer pprof.StopCPUProfile()
	}
	part_1()
	part_2()

}
