package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

const (
	test_input = "seswneswswsenwwnwse"
)

func make_test_vector() [][2]int {
	return [][2]int{
		{5, -10},  // se
		{-5, -10}, // sw
		{5, 10},   // ne
		{-5, -10}, // sw
		{-5, -10}, // sw
		{5, -10},  // se
		{-5, 10},  // nw
		{-10, 0},  // w
		{-5, 10},  // nw
		{5, -10},  // se
	}
}

func TestLineToVectors(t *testing.T) {
	test_vector := line_to_vectors(test_input)
	true_vector := make_test_vector()
	assert.Equal(t, true_vector, test_vector)
}

func TestAddVectors(t *testing.T) {
	test_vec := make_test_vector()
	test_sum := add_vectors(test_vec)
	true_sum := [2]int{-15, -30}
	assert.Equal(t, true_sum, test_sum)
}
func TestVectorToString(t *testing.T) {
	test_vec := [2]int{-15, 30}
	true_str := "-15,30"
	test_str := vec_to_str(test_vec)
	assert.Equal(t, true_str, test_str)
}
func TestLinesToVectorSums(t *testing.T) {
	lines := read_file("input_test.txt")
	test_vecs := lines_to_vectorsums(lines)
	test_sum := test_vecs[2]
	true_sum := [2]int{-15, -30}
	assert.Equal(t, true_sum, test_sum)
}

func TestFlipAndCount(t *testing.T) {
	lines := read_file("input_test.txt")
	test_vecs := lines_to_vectorsums(lines)
	test_map := vectors_to_map(test_vecs)
	test_count := count_true_map(test_map)
	assert.Equal(t, 10, test_count)
}

func TestFlipAndCountDay1(t *testing.T) {
	lines := read_file("input_test.txt")
	test_vecs := lines_to_vectorsums(lines)
	test_map := vectors_to_map(test_vecs)
	test_map = flip_days(test_map, 1)
	test_count := count_true_map(test_map)
	assert.Equal(t, 15, test_count)
}
func TestFlipAndCountDay10(t *testing.T) {
	lines := read_file("input_test.txt")
	test_vecs := lines_to_vectorsums(lines)
	test_map := vectors_to_map(test_vecs)
	test_map = flip_days(test_map, 10)
	test_count := count_true_map(test_map)
	assert.Equal(t, 37, test_count)
}
func TestFlipAndCountDay100(t *testing.T) {
	lines := read_file("input_test.txt")
	test_vecs := lines_to_vectorsums(lines)
	test_map := vectors_to_map(test_vecs)
	test_map = flip_days(test_map, 100)
	test_count := count_true_map(test_map)
	assert.Equal(t, 2208, test_count)
}
