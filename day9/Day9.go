package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
)

func ReadNumbers(r io.Reader) ([]int, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	var output []int
	for scanner.Scan() {
		s_map, _ := strconv.Atoi(scanner.Text())
		output = append(output, s_map)
	}
	return output, scanner.Err()
}

func is_sum_of_2(checksum int, preamble []int) bool {
	for i, ltr := range preamble {
		for j := len(preamble) - 1; j > i; j-- {
			rtl := preamble[j]
			if ltr+rtl == checksum {
				return true
			}
		}
	}
	return false
}

func validate_numbers(input []int, window_size int) int {
	// Sliding window
	for i, j := 0, window_size+1; j < len(input); i, j = i+1, j+1 {
		preamble := input[i:j]
		checksum := input[j]
		if !is_sum_of_2(checksum, preamble) {
			return checksum
		}
	}
	return -1
}

func find_sum(input []int, checksum int) []int {
	for i, val := range input {
		sum := val
		for j := i + 1; ; j++ {
			sum += input[j]
			if sum == checksum {
				return input[i:j]
			}
			if sum > checksum {
				break
			}
		}
	}
	return nil
}

func main() {
	fp, err := os.Open("input_Day9.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	v_numbers, err := ReadNumbers(reader)
	if err != nil {
		fmt.Println(err)
		return
	}
	invalid := validate_numbers(v_numbers, 25)

	fmt.Println(invalid)

	v_sum := find_sum(v_numbers, invalid)

	sort.Ints(v_sum)
	fmt.Println(v_sum[0] + v_sum[len(v_sum)-1])

}
