package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func read_bus_table(r io.Reader) ([]string, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	var output []string
	for scanner.Scan() {
		s_map := scanner.Text()
		output = append(output, s_map)
	}
	return output, scanner.Err()
}

func str_to_ids(str string) []int {
	sep_str := strings.Split(str, ",")
	var out []int
	for _, val := range sep_str {
		if val != "x" {
			id, _ := strconv.Atoi(val)
			out = append(out, id)
		}
	}
	return out
}

func find_earliest_busses(ids []int, time int) map[int]int {
	out := make(map[int]int)
	// n * id = time + delay
	// delay > 0
	// n * id > time
	// n > time / id
	for _, id := range ids {
		// plus one for ceil instead of int div floor
		n := time/id + 1
		delay := n*id - time
		out[id] = delay
	}
	return out
}

func a_min(a, b int) (int, bool) {
	if a < b {
		return a, true
	}
	return b, false
}

func find_shortest_delay(m_bus_delay map[int]int) (int, int) {
	out_id := 0
	out_delay := 99999
	new_min := false
	for id, delay := range m_bus_delay {
		out_delay, new_min = a_min(delay, out_delay)
		if new_min {
			out_id = id
		}
	}
	return out_id, out_delay
}

func str_to_ids_posmap(str string) map[int64]int64 {
	sep_str := strings.Split(str, ",")
	out := make(map[int64]int64)
	for i, val := range sep_str {
		if val != "x" {
			id, _ := strconv.Atoi(val)
			out[int64(i)] = int64(id)
		}
	}
	return out
}

func prod(nums []int64) int64 {
	prd := int64(1)
	for _, x := range nums {
		prd *= x
	}
	return prd
}

func extended_gcd(a int64, b int64) [3]int64 {
	// EEA
	old_r, r := int64(a), int64(b)
	old_s, s := int64(1), int64(0)
	old_t, t := int64(0), int64(1)

	for r != 0 {
		// while r != 0:
		quotient := old_r / r
		old_r, r = r, old_r-quotient*r
		old_s, s = s, old_s-quotient*s
		old_t, t = t, old_t-quotient*t
	}

	// return Bezout coefficient 1, 2, and the gcd
	return [3]int64{old_s, old_t, old_r}
}

func chinese_remainder(remainders []int64, divisors []int64) int64 {
	M := prod(divisors)
	fmt.Println("M: ", M)
	as_ := make([]int64, len(divisors))
	for i, d := range divisors {
		as_[i] = M / d
	}
	eea := make([][3]int64, len(divisors))
	for i, d := range divisors {
		a := as_[i]
		eea[i] = extended_gcd(a, d)
	}
	is_ := make([]int64, len(divisors))
	for i, d := range divisors {
		e := eea[i]
		is_[i] = e[0] % d
	}
	Z := int64(0)
	for i, is := range is_ {
		Z += is * remainders[i] * as_[i]
	}
	fmt.Println("Z: ", Z)
	x := Z % M
	return x
}

func seq_align_timestamp(bus_ids map[int64]int64) int64 {

	rests := make([]int64, len(bus_ids))
	divisors := make([]int64, len(bus_ids))
	i := 0
	for ind, num := range bus_ids {
		rests[i] = (-ind) % num
		divisors[i] = num
		i++
	}

	solution := chinese_remainder(rests, divisors)
	return solution
}

func main() {
	fp, err := os.Open("input_Day13.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	v_strings, err := read_bus_table(reader)
	if err != nil {
		fmt.Println(err)
		return
	}

	earliest_time, _ := strconv.Atoi(v_strings[0])
	s_bus_ids := v_strings[1]

	bus_ids := str_to_ids(s_bus_ids)

	m_bus_delay := find_earliest_busses(bus_ids, earliest_time)

	id, delay := find_shortest_delay(m_bus_delay)

	fmt.Println("[", id, "] = ", delay)

	bus_ids_posmap := str_to_ids_posmap(s_bus_ids)

	ts := seq_align_timestamp(bus_ids_posmap)
	fmt.Println(ts)
}
