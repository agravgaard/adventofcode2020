package main

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"os"
	"strconv"
)

func ReadDirections(r io.Reader) ([]string, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	var output []string
	for scanner.Scan() {
		s_map := scanner.Text()
		output = append(output, s_map)
	}
	return output, scanner.Err()
}

type direction struct {
	north   int
	east    int
	turn    float64
	forward int
}

func deg_to_rad(deg int) float64 {
	return float64(deg) * math.Pi / 180.0
}

func byte_val_to_direction(b byte, val int) direction {
	var out direction
	out.north = 0
	out.east = 0
	out.turn = 0
	out.forward = 0
	switch string(b) {
	case "N":
		out.north = val
	case "S":
		out.north = -val
	case "E":
		out.east = val
	case "W":
		out.east = -val
	case "R":
		out.turn = deg_to_rad(-val)
	case "L":
		out.turn = deg_to_rad(val)
	case "F":
		out.forward = val
	}
	return out
}

func string_to_direction(str string) direction {
	b := str[0]
	val, _ := strconv.Atoi(str[1:])
	return byte_val_to_direction(b, val)
}

func strings_to_directions(v_str []string) []direction {
	var output []direction
	for _, str := range v_str {
		output = append(output, string_to_direction(str))
	}
	return output
}

func add_ne(ds []direction) direction {
	var out direction
	out.north = 0
	out.east = 0
	out.turn = 0
	out.forward = 0
	for _, d := range ds {
		out.north += d.north
		out.east += d.east
	}
	return out
}

func compute_turns(ds []direction) direction {
	var out direction
	out.north = 0
	out.east = 0
	out.turn = 0 // = east
	out.forward = 0
	for _, d := range ds {
		if d.forward != 0 {
			out.east += int(math.Cos(out.turn) * float64(d.forward))
			out.north += int(math.Sin(out.turn) * float64(d.forward))
		} else {
			out.turn += d.turn
		}
	}
	return out
}

func flt_eq_cmp(a, b float64) bool {
	return math.Abs(a-b) < 1e-9
}

const (
	R90 = iota
	L90
	R180
	L180
	Zero
)

func rad_to_dir(rad float64) int {
	if flt_eq_cmp(math.Sin(rad), 1.0) {
		return L90
	} else if flt_eq_cmp(math.Sin(rad), -1.0) {
		return R90
	} else if flt_eq_cmp(rad, math.Pi) {
		return L180
	} else if flt_eq_cmp(rad, -math.Pi) {
		return R180
	}
	return Zero
}

func flip_by_turn(turn float64, n, e int) (int, int) {
	switch rad_to_dir(turn) {
	// return north, east
	case R90:
		return -e, n
	case L90:
		return e, -n
	case R180:
		return -n, -e
	case L180:
		return -n, -e
	}
	return n, e
}

func compute_turns_waypoint(ds []direction) direction {
	var out direction
	out.north = 0
	out.east = 0
	out.turn = 0 // = east
	out.forward = 0
	var wp direction
	wp.north = 1
	wp.east = 10
	wp.turn = 0 // = east
	wp.forward = 0

	for _, d := range ds {
		if d.forward != 0 {
			out.east += d.forward * wp.east
			out.north += d.forward * wp.north
		} else if !flt_eq_cmp(d.turn, 0.0) {
			wp.north, wp.east = flip_by_turn(d.turn, wp.north, wp.east)
		} else {
			wp.north += d.north
			wp.east += d.east
		}
	}
	return out
}

func main() {
	fp, err := os.Open("input_Day12.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	v_strings, err := ReadDirections(reader)
	if err != nil {
		fmt.Println(err)
		return
	}

	ds := strings_to_directions(v_strings)

	nsew_pos := add_ne(ds)

	turn_pos := compute_turns(ds)

	var out direction
	out.north = nsew_pos.north + turn_pos.north
	out.east = nsew_pos.east + turn_pos.east
	out.turn = 0 // = east
	out.forward = 0

	fmt.Println(out)

	out.north = 0
	out.east = 0
	out = compute_turns_waypoint(ds)
	fmt.Println(out)

}
