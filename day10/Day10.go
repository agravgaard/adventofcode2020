package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
)

func ReadNumbers(r io.Reader) ([]int, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	var output []int
	for scanner.Scan() {
		s_map, _ := strconv.Atoi(scanner.Text())
		output = append(output, s_map)
	}
	return output, scanner.Err()
}

func adjescent_diff(input []int) []int {
	output := make([]int, len(input)-1)
	for i, j := 0, 1; j < len(input); i, j = i+1, j+1 {
		output[i] = input[j] - input[i]
	}
	return output
}

func count_val(input []int, val int) int {
	count := 0
	for _, cur_val := range input {
		if cur_val == val {
			count++
		}
	}
	return count
}

func count_combinations_recurse(input []int, ic_map *map[int]uint64, max_len int) uint64 {
	count := uint64(0)
	if len(input) <= 3 {
		return uint64(1)
	}
	key := max_len - len(input)
	if sub_count, ok := (*ic_map)[key]; ok {
		count += sub_count
	} else {
		sub_count := uint64(0)
		for j := 1; j < 4 && j < len(input); j++ {
			if input[j]-input[0] <= 3 {
				sub_input := input[j:]
				sub_count += count_combinations_recurse(sub_input, ic_map, max_len)
			}
		}
		(*ic_map)[key] = sub_count
		count += sub_count
	}
	return count
}

func count_combinations_reverse(input []int) uint64 {
	// index count map
	ic_map := make(map[int]uint64)
	return count_combinations_recurse(input, &ic_map, len(input))
}

func main() {
	fp, err := os.Open("input_Day10.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	v_numbers, err := ReadNumbers(reader)
	if err != nil {
		fmt.Println(err)
		return
	}
	sort.Ints(v_numbers)
	// Find number of adjescent differences of 1 and 3
	adj_diff := adjescent_diff(v_numbers)

	// Add one diff for the first and one 3 for the last
	adj_diff = append(adj_diff, v_numbers[0], 3)
	count_1 := count_val(adj_diff, 1)
	count_3 := count_val(adj_diff, 3)
	fmt.Println(count_1 * count_3)

	// Count number of combinations that would yield adjescent differences no larger than 3
	v_numbers = append(v_numbers, 0, v_numbers[len(v_numbers)-1]+3)
	sort.Ints(v_numbers)
	count := count_combinations_reverse(v_numbers)
	fmt.Println(count)

}
