package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func ReadMap(r io.Reader) ([][]bool, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)
	var result [][]bool
	for scanner.Scan() {
		s_map := scanner.Text()
		var b_map []bool
		for _, letter := range s_map {
			if letter == '#' {
				b_map = append(b_map, true)
			} else {
				b_map = append(b_map, false)
			}
		}
		result = append(result, b_map)
	}
	return result, scanner.Err()
}

func count_trees(vec_map [][]bool, ax uint, ay uint) (int, error) {
	sum_trees := 0
	pos_x := uint(0)
	pos_y := uint(0)
	for {
		pos_x += ax
		pos_x = pos_x % uint(len(vec_map[0]))
		pos_y += ay
		if pos_y >= uint(len(vec_map)) {
			break
		}
		if vec_map[pos_y][pos_x] {
			sum_trees++
		}
	}
	return sum_trees, nil
}

func count_trees_noexcept(vec_map [][]bool, ax uint, ay uint) int {
	count, err := count_trees(vec_map, ax, ay)
	if err != nil {
		fmt.Println(err)
		return 0
	}
	return count
}

func main() {
	fp, err := os.Open("input_Day3.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	vec_map, err := ReadMap(reader)
	if err != nil {
		fmt.Println(err)
		return
	}
	ax := uint(1)
	ay := uint(1)
	count := count_trees_noexcept(vec_map, ax, ay)
	ax = uint(3)
	count *= count_trees_noexcept(vec_map, ax, ay)
	ax = uint(5)
	count *= count_trees_noexcept(vec_map, ax, ay)
	ax = uint(7)
	count *= count_trees_noexcept(vec_map, ax, ay)
	ax = uint(1)
	ay = uint(2)
	count *= count_trees_noexcept(vec_map, ax, ay)

	fmt.Println(count)

}
