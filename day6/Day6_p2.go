package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
)

func count_same(s_val []string) int {
	var same_letters []string
	for _, letter := range s_val[0] {
		same_letters = append(same_letters, string(letter))
	}
	for _, sub_str := range s_val {
		for i := 0; i < len(same_letters); i++ {
			if !strings.Contains(sub_str, same_letters[i]) {
				same_letters = append(same_letters[:i], same_letters[i+1:]...)
				i--
			}
		}
	}
	return len(same_letters)
}

func ReadCountCustomsGroups_p2(r io.Reader) (int, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	sum_counts := 0
	var curr_group []string
	for scanner.Scan() {
		s_map := scanner.Text()
		if s_map == "" {
			sum_counts += count_same(curr_group)
			curr_group = nil
			continue
		}
		curr_group = append(curr_group, s_map)
	}
	return sum_counts, scanner.Err()
}

func main() {
        main_p1()
	fp, err := os.Open("input_Day6.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	sum_counts, err := ReadCountCustomsGroups_p2(reader)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(sum_counts)

}
