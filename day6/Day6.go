package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"

	"github.com/mpvl/unique"
)

func count_different(s_val string) int {
	var v_str []string
	for _, letter := range s_val {
		v_str = append(v_str, string(letter))
	}
	sort.Strings(v_str)
	unique.Strings(&v_str)
	return len(v_str)
}

func ReadCountCustomsGroups(r io.Reader) (int, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	sum_counts := 0
	var curr_group string
	for scanner.Scan() {
		s_map := scanner.Text()
		if s_map == "" {
			sum_counts += count_different(curr_group)
			curr_group = ""
			continue
		}
		curr_group += s_map
	}
	return sum_counts, scanner.Err()
}

func main_p1() {
	fp, err := os.Open("input_Day6.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	sum_counts, err := ReadCountCustomsGroups(reader)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(sum_counts)

}
