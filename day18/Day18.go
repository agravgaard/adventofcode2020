package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func read_lines(r io.Reader) ([]string, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	var out []string
	for scanner.Scan() {
		s_map := scanner.Text()
		out = append(out, s_map)
	}
	return out, scanner.Err()
}

func interpret_expression_ltr(expr string) int {
	split_space := strings.Split(expr, " ")
	lhs := 0
	op := '+'
	for i, str := range split_space {
		if i == 0 {
			lhs, _ = strconv.Atoi(str)
		} else if i%2 == 1 {
			op = rune(str[0])
		} else if i%2 == 0 {
			rhs, _ := strconv.Atoi(str)
			switch op {
			case '+':
				lhs += rhs
			case '*':
				lhs *= rhs
			}
		}
	}
	return lhs
}

func interpret_expression_pm(expr string) int {
	split_mult := strings.Split(expr, " * ")
	mult := 1
	for _, sub_expr := range split_mult {
		split_plus := strings.Split(sub_expr, " + ")
		sum := 0
		for _, s_num := range split_plus {
			num, _ := strconv.Atoi(s_num)
			sum += num
		}
		mult *= sum
	}
	return mult
}

func interpret_expression(expr, precedence string) int {
	switch precedence {
	case "ltr":
		return interpret_expression_ltr(expr)
	case "pm":
		return interpret_expression_pm(expr)
	}
	return 0
}

func interpret_one_inner_parens(line, precedence string) string {
	start := 0
	for i, b := range line {
		if b == '(' {
			start = i + 1
		} else if b == ')' {
			sub_expr := line[start:i]
			result := interpret_expression(sub_expr, precedence)
			s_result := strconv.Itoa(result)
			return line[0:(start-1)] + s_result + line[(i+1):]
		}
	}
	return line
}

func interpret_line(line, precedence string) int {
	for {
		if !strings.ContainsRune(line, '(') {
			break
		}
		line = interpret_one_inner_parens(line, precedence)
	}
	return interpret_expression(line, precedence)
}

func sum_lines(lines []string, precedence string) int {
	sum := 0
	for _, line := range lines {
		sum += interpret_line(line, precedence)
	}
	return sum
}

func main() {
	fp, err := os.Open("input_Day18.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	s_lines, err := read_lines(reader)
	if err != nil {
		fmt.Println(err)
		return
	}

	sum := sum_lines(s_lines, "ltr")
	fmt.Println("Sum = ", sum)

	sum = sum_lines(s_lines, "pm")
	fmt.Println("Sum = ", sum)
}
