package main

import (
	"bufio"
	"fmt"
	"io"
	"os"

	"github.com/mpvl/unique"
)

func read_cubes(r io.Reader) ([]string, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	var out []string
	for scanner.Scan() {
		s_map := scanner.Text()
		out = append(out, s_map)
	}
	return out, scanner.Err()
}

type cube struct {
	x int
	y int
	z int
}

func make_cube(x, y, z int) cube {
	var new_cube cube
	new_cube.x = x
	new_cube.y = y
	new_cube.z = z
	return new_cube
}

func (lhs *cube) lt(rhs cube) bool {
	if lhs.x < rhs.x {
		return true
	}
	if lhs.x == rhs.x {
		if lhs.y < rhs.y {
			return true
		}
		if lhs.y == rhs.y {
			if lhs.z < rhs.z {
				return true
			}
		}
	}
	return false
}

// IntSlice attaches the methods of Interface to []int.
type CubeSlice struct{ P *[]cube }

func (p CubeSlice) Len() int           { return len(*p.P) }
func (p CubeSlice) Swap(i, j int)      { (*p.P)[i], (*p.P)[j] = (*p.P)[j], (*p.P)[i] }
func (p CubeSlice) Less(i, j int) bool { return (*p.P)[i].lt((*p.P)[j]) }
func (p CubeSlice) Truncate(n int)     { *p.P = (*p.P)[:n] }

func get_active_cubes(cubes []string) []cube {
	var out []cube
	for x, s := range cubes {
		for y, b := range s {
			if b == '#' {
				out = append(out, make_cube(x, y, 0))
			}
		}
	}
	return out
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func is_neighbour(c cube, nb cube) bool {
	if abs(c.x-nb.x) <= 1 {
		if abs(c.y-nb.y) <= 1 {
			if abs(c.z-nb.z) <= 1 {
				return true
			}
		}
	}
	return false
}

func is_same(lhs cube, rhs cube) bool {
	if rhs.x == lhs.x && rhs.y == lhs.y && rhs.z == lhs.z {
		return true
	}
	return false
}

func rules(is_activated bool, n_neighbours int) bool {
	if !is_activated && n_neighbours == 3 {
		return true
	} else if is_activated && (n_neighbours == 2 || n_neighbours == 3) {
		return true
	}
	return false
}

func generate_neighbours(c cube) []cube {
	out := make([]cube, 27)
	i := 0
	for x := c.x - 1; x <= c.x+1; x++ {
		for y := c.y - 1; y <= c.y+1; y++ {
			for z := c.z - 1; z <= c.z+1; z++ {
				out[i].x = x
				out[i].y = y
				out[i].z = z
				i++
			}
		}
	}
	return out
}

func activate_neighbors(c cube, cubes []cube) []cube {
	var out []cube
	v_nb := generate_neighbours(c)
	for _, nb := range v_nb {
		n_neighbours := 0
		is_activated := false
		if is_same(c, nb) {
			is_activated = true
			n_neighbours--
		}
		skip := false
		for _, pot_nb := range cubes {
			if is_neighbour(pot_nb, nb) {
				n_neighbours++
			}
			if !is_activated && is_same(pot_nb, nb) {
				skip = true
				break
			}

		}
		if !skip {
			activated := rules(is_activated, n_neighbours)
			if activated {
				out = append(out, nb)
			}
		}
	}
	return out
}

func cycle(cubes []cube) []cube {
	var out []cube
	for _, c := range cubes {
		new_cubes := activate_neighbors(c, cubes)
		out = append(out, new_cubes...)
	}
	unique.Sort(CubeSlice{&out})
	unique.Unique(CubeSlice{&out})
	return out
}

func run_n_cycles(cubes []cube, n int) []cube {
	for i := 0; i < n; i++ {
		cubes = cycle(cubes)
	}
	return cubes
}

func main_p1() {
	fp, err := os.Open("input_Day17.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	s_cubes, err := read_cubes(reader)
	if err != nil {
		fmt.Println(err)
		return
	}

	cubes := get_active_cubes(s_cubes)
	if len(cubes) != 29 {
		fmt.Println("not expected len of cubes:", len(cubes))
	}
	cubes = run_n_cycles(cubes, 6)
	fmt.Println("Cubes activated:", len(cubes))

}
