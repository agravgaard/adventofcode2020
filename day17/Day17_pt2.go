package main

import (
	"fmt"
	"io"
	"os"

	"github.com/mpvl/unique"
)

type hypercube struct {
	x int
	y int
	z int
	w int
}

func make_hypercube(x, y, z, w int) hypercube {
	var new_hypercube hypercube
	new_hypercube.x = x
	new_hypercube.y = y
	new_hypercube.z = z
	new_hypercube.w = w
	return new_hypercube
}

func (lhs *hypercube) lt(rhs hypercube) bool {
	if lhs.x < rhs.x {
		return true
	}
	if lhs.x == rhs.x {
		if lhs.y < rhs.y {
			return true
		}
		if lhs.y == rhs.y {
			if lhs.z < rhs.z {
				return true
			}
			if lhs.z == rhs.z {
				if lhs.w < rhs.w {
					return true
				}
			}
		}
	}
	return false
}

// IntSlice attaches the methods of Interface to []int.
type hypercubeSlice struct{ P *[]hypercube }

func (p hypercubeSlice) Len() int           { return len(*p.P) }
func (p hypercubeSlice) Swap(i, j int)      { (*p.P)[i], (*p.P)[j] = (*p.P)[j], (*p.P)[i] }
func (p hypercubeSlice) Less(i, j int) bool { return (*p.P)[i].lt((*p.P)[j]) }
func (p hypercubeSlice) Truncate(n int)     { *p.P = (*p.P)[:n] }

func get_active_hypercubes(hypercubes []string) []hypercube {
	var out []hypercube
	for x, s := range hypercubes {
		for y, b := range s {
			if b == '#' {
				out = append(out, make_hypercube(x, y, 0, 0))
			}
		}
	}
	return out
}

func is_hneighbour(c hypercube, nb hypercube) bool {
	if abs(c.x-nb.x) <= 1 {
		if abs(c.y-nb.y) <= 1 {
			if abs(c.z-nb.z) <= 1 {
				if abs(c.w-nb.w) <= 1 {
					return true
				}
			}
		}
	}
	return false
}

func is_hsame(lhs hypercube, rhs hypercube) bool {
	if rhs.x == lhs.x && rhs.y == lhs.y && rhs.z == lhs.z && rhs.w == lhs.w {
		return true
	}
	return false
}

func generate_hneighbours(c hypercube) []hypercube {
	out := make([]hypercube, 3*3*3*3)
	i := 0
	for x := c.x - 1; x <= c.x+1; x++ {
		for y := c.y - 1; y <= c.y+1; y++ {
			for z := c.z - 1; z <= c.z+1; z++ {
				for w := c.w - 1; w <= c.w+1; w++ {
					out[i].x = x
					out[i].y = y
					out[i].z = z
					out[i].w = w
					i++
				}
			}
		}
	}
	return out
}

func activate_hneighbors(c hypercube, hypercubes []hypercube) []hypercube {
	var out []hypercube
	v_nb := generate_hneighbours(c)
	for _, nb := range v_nb {
		n_neighbours := 0
		is_activated := false
		if is_hsame(c, nb) {
			is_activated = true
			n_neighbours--
		}
		skip := false
		for _, pot_nb := range hypercubes {
			if is_hneighbour(pot_nb, nb) {
				n_neighbours++
			}
			if !is_activated && is_hsame(pot_nb, nb) {
				skip = true
				break
			}

		}
		if !skip {
			activated := rules(is_activated, n_neighbours)
			if activated {
				out = append(out, nb)
			}
		}
	}
	return out
}

func hcycle(hypercubes []hypercube) []hypercube {
	var out []hypercube
	for _, c := range hypercubes {
		new_hypercubes := activate_hneighbors(c, hypercubes)
		out = append(out, new_hypercubes...)
	}
	unique.Sort(hypercubeSlice{&out})
	unique.Unique(hypercubeSlice{&out})
	return out
}

func run_n_hcycles(hypercubes []hypercube, n int) []hypercube {
	for i := 0; i < n; i++ {
		hypercubes = hcycle(hypercubes)
	}
	return hypercubes
}

func main() {
	main_p1()

	fp, err := os.Open("input_Day17.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	s_hypercubes, err := read_cubes(reader)
	if err != nil {
		fmt.Println(err)
		return
	}

	hypercubes := get_active_hypercubes(s_hypercubes)
	if len(hypercubes) != 29 {
		fmt.Println("not expected len of hypercubes:", len(hypercubes))
	}
	hypercubes = run_n_hcycles(hypercubes, 6)

	fmt.Println("hypercubes activated:", len(hypercubes))

}
