package main

import (
	"fmt"
)

// map[number] = last_pos
func next_number_map(numbers *map[uint64]uint64, last_number uint64, i uint64) uint64 {
	if pos, ok := (*numbers)[last_number]; ok {
		return i - pos
	}
	return 0
}

func run_until_map(start_numbers map[uint64]uint64, limit uint64) uint64 {
	next_num := uint64(4)
	prev_num := uint64(0)
	numbers := start_numbers
	percent_test := uint64(0)
	fmt.Println("Start i:", uint64(len(start_numbers)-1))
	for i := uint64(len(start_numbers) - 1); i < limit; i++ {
		prev_num = next_num
		next_num = next_number_map(&numbers, prev_num, i)
		numbers[prev_num] = i
		if (i*100)/limit == percent_test {
			percent_test += 10
			fmt.Println((i*100)/limit, "% cur_num:", prev_num, " i:", i)
		}
	}
	return prev_num
}

func main() {
	starting_numbers := []uint64{1, 0, 16, 5, 17, 4}
	start_map := make(map[uint64]uint64)
	for pos, number := range starting_numbers {
		start_map[number] = uint64(pos)
	}

	last_number := run_until_map(start_map, 2020)
	if 1294 != last_number {
		fmt.Println("We made a mistake")
	}
	fmt.Println(last_number)

	start_map_2 := make(map[uint64]uint64)
	for pos, number := range starting_numbers {
		start_map_2[number] = uint64(pos)
	}
	last_number = run_until_map(start_map_2, 30000000)
	fmt.Println(last_number)
}
