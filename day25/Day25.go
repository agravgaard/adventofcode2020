package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime/pprof"
)

const (
	input_dk = 15113849
	input_ck = 4206373
)

func part_1() {
	enc_key := caluculate_encryption_key(input_ck, input_dk)
	fmt.Println("Part 1, Enc. Key:", enc_key)

}

var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")

func main() {
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		err = pprof.StartCPUProfile(f)
		if err != nil {
			log.Fatal(err)
		}
		defer pprof.StopCPUProfile()
	}
	part_1()

}
