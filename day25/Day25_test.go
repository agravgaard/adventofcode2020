package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

const (
	test_door_pk = 17807724
	test_card_pk = 5764801
	test_card_ls = 8
	test_door_ls = 11
)

func TestDetermineLoopSize(t *testing.T) {
	test_ls := determine_loop_size(test_card_pk, 7)
	assert.Equal(t, test_card_ls, test_ls)

	test_ls = determine_loop_size(test_door_pk, 7)
	assert.Equal(t, test_door_ls, test_ls)
}
func TestDetermineEncryptionKey(t *testing.T) {
	true_ek := 14897079

	test_dek := determine_enc_key(test_card_ls, test_door_pk)
	assert.Equal(t, true_ek, test_dek)

	test_cek := determine_enc_key(test_door_ls, test_card_pk)
	assert.Equal(t, true_ek, test_cek)

	assert.Equal(t, test_dek, test_cek)
}

func TestCaluculateEncryptionKey(t *testing.T) {
	test_key := caluculate_encryption_key(test_door_pk, test_card_pk)
	true_key := 14897079
	assert.Equal(t, true_key, test_key)
}
