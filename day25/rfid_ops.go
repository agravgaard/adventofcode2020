package main

const (
	remainder = 20201227
)

func determine_loop_size(key, subject_number int) int {
	value := 1
	for loop_size := 1; ; loop_size++ {
		value *= subject_number
		value = value % remainder
		if value == key {
			return loop_size
		}
	}
}
func determine_enc_key(loop_size, subject_number int) int {
	value := 1
	for i := 0; i < loop_size; i++ {
		value *= subject_number
		value = value % remainder
	}
	return value
}

func caluculate_encryption_key(key_a, key_b int) int {
	subject_number := 7
	a_loop_size := determine_loop_size(key_a, subject_number)
	// b_loop_size := determine_loop_size(key_b, subject_number)
	return determine_enc_key(a_loop_size, key_b)
}
