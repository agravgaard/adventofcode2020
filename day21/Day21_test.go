package main

import (
	"fmt"
	"io"
	"os"
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"
	// "github.com/stretchr/testify/mock"
	// "github.com/stretchr/testify/require"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

func make_test_str() []string {
	return []string{
		"mxmxvkd kfcds sqjhc nhms (contains dairy, fish)",
		"trh fvjkl sbzzf mxmxvkd (contains dairy)",
		"sqjhc fvjkl (contains soy)",
		"sqjhc mxmxvkd sbzzf (contains fish)",
	}
}
func make_test_foods() []food {
	out := make([]food, 4)
	out[0].ingr = []string{"mxmxvkd", "kfcds", "sqjhc", "nhms"}
	out[0].alrg = []string{"dairy", "fish"}
	out[1].ingr = []string{"trh", "fvjkl", "sbzzf", "mxmxvkd"}
	out[1].alrg = []string{"dairy"}
	out[2].ingr = []string{"sqjhc", "fvjkl"}
	out[2].alrg = []string{"soy"}
	out[3].ingr = []string{"sqjhc", "mxmxvkd", "sbzzf"}
	out[3].alrg = []string{"fish"}
	return out
}

func TestReader(t *testing.T) {
	fp, err := os.Open("input_test.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	s_foods, err := read_foods(reader)
	if err != nil {
		fmt.Println(err)
		return
	}
	true_s_foods := make_test_str()

	assert.Equal(t, true_s_foods, s_foods)
}

func TestMakeFoods(t *testing.T) {
	s_foods := make_test_str()
	foods := strings_to_foods(s_foods)
	assert.Equal(t, len(s_foods), len(foods))
	true_foods := make_test_foods()

	for i, fd := range true_foods {
		assert.Equal(t, fd, foods[i])
	}
}

func TestCountAppearances(t *testing.T) {
	foods := make_test_foods()

	test_str := []string{}
	count := count_appearances(test_str, foods)
	assert.Equal(t, uint(0), count)

	test_str = []string{"sbzzf"}
	count = count_appearances(test_str, foods)
	assert.Equal(t, uint(2), count)

	test_str = []string{"kfcds", "nhms", "sbzzf", "trh"}
	count = count_appearances(test_str, foods)
	assert.Equal(t, uint(5), count)
}

func TestCreateIngredients(t *testing.T) {
	foods := make_test_foods()
	ingrs := create_ingredients(foods)
	true_ingr_ingr := []string{
		"mxmxvkd", "kfcds", "sqjhc", "nhms",
		"trh", "fvjkl", "sbzzf"}
	sort.Strings(true_ingr_ingr)
	for i, ingr := range ingrs {
		assert.Equal(t, true_ingr_ingr[i], ingr.ingr)
		assert.Empty(t, ingr.potential_alrg)
	}
}
func TestDetermineAlrgFree(t *testing.T) {
	foods := make_test_foods()
	true_str := []string{"kfcds", "nhms", "sbzzf", "trh"}
	test_str := determine_allergenfree_ingredients(foods)
	assert.Equal(t, true_str, test_str)
}
func TestDetermineAlrgDanger(t *testing.T) {
	foods := make_test_foods()
	true_str := []string{"mxmxvkd","sqjhc","fvjkl"}
	test_str := determine_allergendanger_ingredients(foods)
	assert.Equal(t, true_str, test_str)
}
