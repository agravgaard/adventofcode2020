package main

import (
	"fmt"
	"sort"
	"strings"

	"github.com/mpvl/unique"
)

type food struct {
	ingr []string
	alrg []string
}

func str_to_food(str string) food {
	var out food
	s_ingr_alrg := strings.Split(str, "(contains ")

	s_ingr := strings.TrimRight(s_ingr_alrg[0], " ")
	out.ingr = strings.Split(s_ingr, " ")

	s_alrg := strings.TrimRight(s_ingr_alrg[1], ")")
	out.alrg = strings.Split(s_alrg, ", ")
	return out
}

func get_all_ingr(foods []food) []string {
	var out_ingr []string
	for _, vstr := range foods {
		out_ingr = append(out_ingr, vstr.ingr...)
	}
	sort.Strings(out_ingr)
	unique.Strings(&out_ingr)

	return out_ingr
}

type ingredient struct {
	ingr           string
	potential_alrg []string
}
func (lhs *ingredient) lt(rhs ingredient) bool {
	return lhs.potential_alrg[0] < rhs.potential_alrg[0]
}

type IngredientSlice struct{ P *[]ingredient }
func (p IngredientSlice) Len() int           { return len(*p.P) }
func (p IngredientSlice) Swap(i, j int)      { (*p.P)[i], (*p.P)[j] = (*p.P)[j], (*p.P)[i] }
func (p IngredientSlice) Less(i, j int) bool { return (*p.P)[i].lt((*p.P)[j]) }
func (p IngredientSlice) Truncate(n int)     { *p.P = (*p.P)[:n] }


func create_ingredients(foods []food) []ingredient {
	all_ingr := get_all_ingr(foods)
	var out []ingredient
	for _, ingr := range all_ingr {
		out = append(out, ingredient{ingr, []string{}})
	}
	return out
}
func find_pot_alrgs(ingr string, foods []food) []string {
	var pot_alrgs []string
	for _, fd := range foods {
		if str_in_vstr(ingr, fd.ingr) {
			pot_alrgs = append(pot_alrgs, fd.alrg...)
		}
	}
	sort.Strings(pot_alrgs)
	unique.Strings(&pot_alrgs)
	return pot_alrgs
}
func has_unique_alrg(ingr ingredient, ingrs []ingredient) (bool, string) {
	for _, pa := range ingr.potential_alrg {
		is_unique := true
		for _, other := range ingrs {
			if other.ingr == ingr.ingr {
				continue
			}
			if str_in_vstr(pa, other.potential_alrg) {
				is_unique = false
				break
			}
		}
		if is_unique {
			return true, pa
		}
	}
	return false, ""
}

func is_one_or_zero_alrg(ingr ingredient) bool {
	return len(ingr.potential_alrg) <= 1
}

func all_1_or_0_alrg(ingrs []ingredient) bool {
	for _, ingr := range ingrs {
		if !is_one_or_zero_alrg(ingr) {
			return false
		}
	}
	return true
}

func remove_idx(slice []string, s int) []string {
	return append(slice[:s], slice[s+1:]...)
}

func remove_indicies(slice []string, s []int) []string {
	sort.Ints(s)
	for i := len(s) - 1; i >= 0; i-- {
		slice = remove_idx(slice, s[i])
	}
	return slice
}
func remove_matches(match []string, ingrs *[]ingredient) {
	for i, ingr := range *ingrs {
		if is_one_or_zero_alrg(ingr) {
			continue
		}
		var a_to_rm []int
		for a, al := range ingr.potential_alrg {
			if str_in_vstr(al, match) {
				a_to_rm = append(a_to_rm, a)
			}
		}
		(*ingrs)[i].potential_alrg = remove_indicies(ingr.potential_alrg, a_to_rm)
	}
}

func appears_without(al, ingr string, foods []food) bool {
	for _, fd := range foods {
		if str_in_vstr(al, fd.alrg) {
			if !str_in_vstr(ingr, fd.ingr) {
				return true
			}
		}
	}
	return false
}

func reduce_uniques(ingrs *[]ingredient, foods []food) {
	// if an allergen appears in a food,
	//  without the ingredient in the list,
	//  the ingredient can't have it
	for i, ingr := range *ingrs {
		var al_to_rm []int
		for j, al := range ingr.potential_alrg {
			if appears_without(al, ingr.ingr, foods) {
				al_to_rm = append(al_to_rm, j)
			}
		}
		(*ingrs)[i].potential_alrg = remove_indicies(ingr.potential_alrg, al_to_rm)
	}

}
func remove_and_reduce_uniques(ingrs *[]ingredient, foods []food) {
	for {
		reduce_uniques(ingrs, foods)
		var new_uniques []string
		for i, ingr := range *ingrs {
			b, al := has_unique_alrg(ingr, *ingrs)
			if b {
				(*ingrs)[i].potential_alrg = []string{al}
				new_uniques = append(new_uniques, al)
			}
		}
		if len(new_uniques) == 0 {
			fmt.Println("oh no, 0 new uniques")
			break
		}
		remove_matches(new_uniques, ingrs)

		if all_1_or_0_alrg(*ingrs) {
			break
		}
	}
}
func determine_allergenfree_ingredients(foods []food) []string {
	ingrs := create_ingredients(foods)
	for i, ingr := range ingrs {
		ingrs[i].potential_alrg = find_pot_alrgs(ingr.ingr, foods)
	}
	remove_and_reduce_uniques(&ingrs, foods)
	var out []string
	for _, ingr := range ingrs {
		if len(ingr.potential_alrg) == 0 {
			out = append(out, ingr.ingr)
		}
	}
	return out
}
func determine_allergendanger_ingredients(foods []food) []string {
	ingrs := create_ingredients(foods)
	for i, ingr := range ingrs {
		ingrs[i].potential_alrg = find_pot_alrgs(ingr.ingr, foods)
	}
	remove_and_reduce_uniques(&ingrs, foods)
	var danger_list []ingredient
	for _, ingr := range ingrs {
		if len(ingr.potential_alrg) == 1 {
			danger_list = append(danger_list, ingr)
		}
	}
        unique.Sort(IngredientSlice{&danger_list})
        out := make([]string, len(danger_list))
        for i, dl := range danger_list {
            out[i] = dl.ingr
        }

	return out
}

func str_in_vstr(str string, vstr []string) bool {
	for _, v_str := range vstr {
		if str == v_str {
			return true
		}
	}
	return false
}
func count_appearances(alrg_free_ingr []string, foods []food) uint {
	count := uint(0)
	for _, fd := range foods {
		for _, ingr := range fd.ingr {
			if str_in_vstr(ingr, alrg_free_ingr) {
				count++
			}
		}
	}
	return count
}
