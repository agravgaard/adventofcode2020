package main

import (
	"bufio"
	"io"
)

func read_foods(r io.Reader) ([]string, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	var out []string
	for scanner.Scan() {
		s_map := scanner.Text()
		out = append(out, s_map)
	}
	return out, scanner.Err()
}

func strings_to_foods(vstr []string) []food {
	out := make([]food, len(vstr))
	for i, s_food := range vstr {
		if len(s_food) != 0 {
			out[i] = str_to_food(s_food)
		}
	}
	return out
}
