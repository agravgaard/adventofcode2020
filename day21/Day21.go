package main

import (
	"fmt"
	"io"
	"os"
	"strings"
)

func main() {
	fp, err := os.Open("input_Day21.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	vs_foods, err := read_foods(reader)
	if err != nil {
		fmt.Println(err)
		return
	}

	foods := strings_to_foods(vs_foods)

	alrg_free_ingr := determine_allergenfree_ingredients(foods)
	count := count_appearances(alrg_free_ingr, foods)
	fmt.Println("Alrg free appearances:", count)
	alrg_danger_ingr := determine_allergendanger_ingredients(foods)
	fmt.Println("Alrg danger list:", strings.Join(alrg_danger_ingr, ","))

}
