package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"regexp"
	"strconv"
	"strings"
)

func read_lines(r io.Reader) ([]string, []string, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	var out_one []string
	for scanner.Scan() {
		s_map := scanner.Text()
		if s_map == "" {
			break
		}
		out_one = append(out_one, s_map)
	}
	var out_two []string
	for scanner.Scan() {
		s_map := scanner.Text()
		out_two = append(out_two, s_map)
	}
	return out_one, out_two, scanner.Err()
}

func recurse_rule_str(rule string, rules map[uint]string) string {
	out_str := ""
	split_str := strings.Split(rule, " ")
	for _, s_num := range split_str {
		if s_num == "" {
			continue
		}
		num, err := strconv.Atoi(s_num)
		if err != nil {
			fmt.Println(err)
		}
		out_str += recurse_rule(uint(num), rules)
	}
	return out_str
}

func recurse_rule(rl_num uint, rules map[uint]string) string {
	s_rule := rules[rl_num]
	if strings.ContainsRune(s_rule, '"') {
		return string(s_rule[1]) // assume s_rule = \"x\"
	}

	self_num := " " + strconv.Itoa(int(rl_num))
	if strings.Contains(s_rule, self_num) {
		if rl_num == 8 {
			// rules[8] = "42 | 42 8"
			return "(" + recurse_rule(42, rules) + "){1,}?"
                        // 1 or more, prefer fewer
		} else if rl_num == 11 {
			// rules[11] = "42 31 | 42 11 31"
			rl_42 := "(" + recurse_rule(42, rules) + ")"
			rl_31 := "(" + recurse_rule(31, rules) + ")"
			out_str := "(" + rl_42 + rl_31 + ")"
			for i := 2; i <= 11; i++ {
				s_i := strconv.Itoa(i)
				num := "{" + s_i + "}"
				out_str += "|(" + rl_42 + num + rl_31 + num + ")"
			}
			return out_str
		}
	}
	if strings.ContainsRune(s_rule, '|') {
		out_str := "("
		// or type rule
		lhs_rhs := strings.Split(s_rule, " | ")
		out_str += recurse_rule_str(lhs_rhs[0], rules)
		out_str += "|"
		out_str += recurse_rule_str(lhs_rhs[1], rules)
		return out_str + ")"
	}
	return recurse_rule_str(s_rule, rules)
}

func parse_from(from_rule string, rules map[uint]string) string {
	// Assume no OR in from_rule
	main_rules := strings.Split(from_rule, " ")
	out_rule := ""
	for _, s_rl_num := range main_rules {
		out_rule += "("
		rl_num, err := strconv.Atoi(s_rl_num)
		if err != nil {
			fmt.Println(err)
		}
		out_rule += recurse_rule(uint(rl_num), rules)
		out_rule += ")"
	}
	return out_rule

}

func parse_rules(rules map[uint]string, main_i uint) string {
	main_rule := rules[main_i]
	return parse_from(main_rule, rules)
}

func parse_intc_str(str string) (uint, string) {
	split_str := strings.Split(str, ": ")
	i, _ := strconv.Atoi(split_str[0])
	return uint(i), split_str[1]
}

func generate_rules(s_rules []string) map[uint]string {
	rules := make(map[uint]string)
	for _, rule := range s_rules {
		i, str := parse_intc_str(rule)
		rules[i] = str
	}
	return rules
}
func generate_regex(rules map[uint]string) string {
	rule := parse_rules(rules, 0)
	return rule
}

func num_matches(s_messages []string, regex string) uint {
	rxp, err := regexp.Compile(regex)
	if err != nil {
		fmt.Println(err)
	}
	sum := uint(0)
	for _, message := range s_messages {
		loc := rxp.FindStringIndex(message)
		if len(loc) != 0 && loc[0] == 0 && loc[1] == len(message) {
			sum++
		}
	}
	return sum
}

func main() {
	fp, err := os.Open("input_Day19.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	s_rules, s_messages, err := read_lines(reader)
	if err != nil {
		fmt.Println(err)
		return
	}
	rules := generate_rules(s_rules)

	// Part 1
	regex := generate_regex(rules)
	sum := num_matches(s_messages, regex)
	fmt.Println("Sum = ", sum)

	// Part 2
	rules[8] = "42 | 42 8"
	rules[11] = "42 31 | 42 11 31"
	rxp_42, _ := regexp.Compile(recurse_rule(42, rules))
	rxp_31, _ := regexp.Compile(recurse_rule(31, rules))
	longest_42 := 0
	longest_31 := 0
	for _, message := range s_messages {
		m42 := rxp_42.FindAllString(message, -1)
		if len(m42) > longest_42 {
			longest_42 = len(m42)
		}
		m31 := rxp_31.FindAllString(message, -1)
		if len(m31) > longest_31 {
			longest_31 = len(m31)
		}
	}
	fmt.Println("longest 42:", longest_42, ", 31:", longest_31)

	regex = generate_regex(rules)
	sum = num_matches(s_messages, regex)
	fmt.Println("Sum = ", sum)

}
