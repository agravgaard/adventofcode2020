package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMain(m *testing.M) {
	os.Exit(m.Run())
}

const (
	test_input = "389125467"
)

func make_test_cups() []int {
	return []int{3, 8, 9, 1, 2, 5, 4, 6, 7}
}

func TestMakeCups(t *testing.T) {
	test_cups := make_cups(test_input)
	true_cups := make_test_cups()
	assert.Equal(t, true_cups, test_cups)
}
func TestMakeString(t *testing.T) {
	test_cups := make_test_cups()
	test_str := make_string(test_cups)
	assert.Equal(t, test_input, test_str)
}
func TestRotate(t *testing.T) {
	test_rot_cups := []int{9, 2, 6, 5, 8, 3, 7, 4}
	true_rot_cups := []int{2, 6, 5, 8, 3, 7, 4, 9}
	rotate(&test_rot_cups)
	assert.Equal(t, true_rot_cups, test_rot_cups)
}
func TestRotateN(t *testing.T) {
	test_rot_cups := []int{9, 2, 6, 5, 8, 3, 7, 4}
	true_rot_cups := []int{5, 8, 3, 7, 4, 9, 2, 6}
	rotate_n(&test_rot_cups, 3)
	assert.Equal(t, true_rot_cups, test_rot_cups)
}
func TestRotateN0(t *testing.T) {
	test_rot_cups := []int{9, 2, 6, 5, 8, 3, 7, 4}
	true_rot_cups := []int{9, 2, 6, 5, 8, 3, 7, 4}
	rotate_n(&test_rot_cups, 0)
	assert.Equal(t, true_rot_cups, test_rot_cups)
}
func TestPickUpAndInsertRot(t *testing.T) {
	test_cups := []int{9, 2, 6, 5, 8, 3, 7, 4}
	true_cups := []int{8, 2, 6, 5, 3, 7, 4, 9}
	pick_up_and_insert_after(8, &test_cups, 9)
	assert.Equal(t, true_cups, test_cups)
}
func TestGetMax(t *testing.T) {
	test_cups := []int{9, 2, 6, 5, 8, 3, 7, 4}
	test_max := get_max(test_cups)
	assert.Equal(t, test_max, 9)
}

func TestSimulateMoves10(t *testing.T) {
	test_cups := make_test_cups()
	final_cups := simulate_n_moves(test_cups, uint(10))
	true_final_cups := []int{9, 2, 6, 5, 8, 3, 7, 4}
	assert.Equal(t, true_final_cups, final_cups)
}
func TestSimulateMoves100(t *testing.T) {
	test_cups := make_test_cups()
	final_cups := simulate_n_moves(test_cups, uint(100))
	true_final_cups := []int{6, 7, 3, 8, 4, 5, 2, 9}
	assert.Equal(t, true_final_cups, final_cups)
}

/* // takes too long...
func TestSimulateMoves10MWith1MCups(t *testing.T) {
	test_cups := make_test_cups()
	test_1M_cups := make_1M_cups(test_cups)
	final_cups := simulate_n_moves(test_1M_cups, uint(10000000))
	true_final_cups := []int{934001, 159792}
	assert.Equal(t, true_final_cups, final_cups[:2])
}
*/


// The ring method is even slower, but kept for future reference.
func TestSimulateMoves10Ring(t *testing.T) {
	test_cups := make_test_cups()
	final_cups := simulate_n_moves_ring(test_cups, uint(10))
	true_final_cups := []int{9, 2, 6, 5, 8, 3, 7, 4}
	assert.Equal(t, true_final_cups, final_cups)
}
func TestSimulateMoves100Ring(t *testing.T) {
	test_cups := make_test_cups()
	final_cups := simulate_n_moves_ring(test_cups, uint(100))
	true_final_cups := []int{6, 7, 3, 8, 4, 5, 2, 9}
	assert.Equal(t, true_final_cups, final_cups)
}
