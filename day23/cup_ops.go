package main

import (
	"strconv"
	"strings"
)

func make_cups(input string) []int {
	out := make([]int, len(input))
	for i, b := range input {
		out[i], _ = strconv.Atoi(string(b))
	}
	return out
}

func make_string(input []int) string {
	out := make([]string, len(input))
	for i, b := range input {
		out[i] = strconv.Itoa(b)
	}
	return strings.Join(out, "")
}

// pick up and insert should be combined for less alloc

func pick_up_and_insert_after(after int, cups *[]int, max int) {
	pick_up := make([]int, 3)
	copy(pick_up, (*cups)[1:4])
	*cups = append((*cups)[4:], (*cups)[0])
	for ; ; after-- {
		if after < 1 {
			after = max
		}
		for i, cup := range *cups {
			if after == cup {
				*cups = append((*cups)[:i+1], append(pick_up, (*cups)[i+1:]...)...)
				return
			}
		}
	}
}

func rotate(cups *[]int) {
	*cups = append((*cups)[1:], (*cups)[0])
}
func rotate_n(cups *[]int, n int) {
	*cups = append((*cups)[n:], (*cups)[:n]...)
}

func get_max(cups []int) int {
	out := 0
	for _, c := range cups {
		if c > out {
			out = c
		}
	}
	return out
}

func perform_move(cups *[]int, max int) {
	current := (*cups)[0]
	pick_up_and_insert_after(current-1, cups, max)
}

func from_1(cups *[]int) {
	for i, c := range *cups {
		if c == 1 {
			rotate_n(cups, i)
			*cups = (*cups)[1:]
			return
		}
	}
}

func make_1M_cups(cups []int) []int {
	out := make([]int, 1000000)
	copy(out[:len(cups)], cups)
	start := get_max(cups) + 1
	for i := len(cups); i < len(out); i++ {
		out[i] = start
		start++
	}
	return out
}

func simulate_n_moves(cups []int, n uint) []int {
	max := get_max(cups)
	for i := uint(0); i < n; i++ {
		perform_move(&cups, max)
	}

	from_1(&cups)
	return cups
}
