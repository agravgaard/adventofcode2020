package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime/pprof"
)

const (
	input = "362981754"
)

func part_1() {
	cups := make_cups(input)
	final_cups := simulate_n_moves(cups, 100)
	fmt.Println("Final cups, part 1:", make_string(final_cups))
}
func part_2() {
	cups := make_cups(input)
	cups = make_1M_cups(cups)
	final_cups := simulate_n_moves(cups, 10000000)
	fmt.Println("Final cups, part 2:", final_cups[:2])
	fmt.Println("Mult:", final_cups[0]*final_cups[1])
}

var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")

func main() {
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		err = pprof.StartCPUProfile(f)
		if err != nil {
			log.Fatal(err)
		}
		defer pprof.StopCPUProfile()
	}
	part_1()
	part_2()

}
