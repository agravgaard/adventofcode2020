package main

import "container/ring"

func perform_move_ring(cup_ring *ring.Ring, max int) *ring.Ring {
	after := cup_ring.Value.(int) - 1

	pick_up := cup_ring.Unlink(3)
	cup_ring = cup_ring.Next()

	for ; ; after-- {
		if after < 1 {
			after = max
		}
		for i := 0; i < cup_ring.Len(); i++ {
			if after == cup_ring.Value.(int) {
				cup_ring.Link(pick_up)
				cup_ring = cup_ring.Move(-i)
				return cup_ring
			}
			cup_ring = cup_ring.Next()
		}
	}
}

func from_1_ring(cup_ring *ring.Ring, cups *[]int) {
	for i := 0; i < cup_ring.Len(); i++ {
		if cup_ring.Value == 1 {
			break
		}
		cup_ring = cup_ring.Next()
	}
	copy_ring_to_intslice(cup_ring, cups)
	*cups = (*cups)[1:]
}

func copy_ring_to_intslice(cp_ring *ring.Ring, slc *[]int) {
	for i := 0; i < cp_ring.Len(); i++ {
		(*slc)[i] = cp_ring.Value.(int)
		cp_ring = cp_ring.Next()
	}
}
func intslice_to_ring(slc []int) *ring.Ring {
	n := len(slc)
	r := ring.New(n)
	for i := 0; i < n; i++ {
		r.Value = slc[i]
		r = r.Next()
	}
	return r
}

func simulate_n_moves_ring(cups []int, n uint) []int {
	max := get_max(cups)
	cup_ring := intslice_to_ring(cups)
	for i := uint(0); i < n; i++ {
		cup_ring = perform_move_ring(cup_ring, max)
	}

	from_1_ring(cup_ring, &cups)
	return cups
}
