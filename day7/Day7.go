package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"

	"github.com/mpvl/unique"
)

func ReadRules(r io.Reader) ([]string, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	var output []string
	for scanner.Scan() {
		s_map := scanner.Text()
		output = append(output, s_map)
	}
	return output, scanner.Err()
}

type bag struct {
	tone  string
	color string
}

type rule struct {
	carrier bag
	carries map[bag]int
}

func parse_rule(s_rule string) rule {
	vs_rule := strings.Split(s_rule, " ")
	var out_rule rule
	// 0=tone 1=color
	out_rule.carrier.tone = vs_rule[0]
	out_rule.carrier.color = vs_rule[1]
	// 2=bags 3=carries
	out_rule.carries = make(map[bag]int)
	if vs_rule[4] == "no" {
		return out_rule
	}
	var err error
	// 4=Number 5=tone 6=color 7=bag[s,] [repeat from 4]
	for i := 4; i < len(vs_rule); i += 4 {
		var cur_bag bag
		cur_bag.tone = vs_rule[i+1]
		cur_bag.color = vs_rule[i+2]
		out_rule.carries[cur_bag], err = strconv.Atoi(vs_rule[i])
		if err != nil {
			fmt.Println(err)
		}
	}
	return out_rule
}

func parse_rules(str_rules []string) []rule {
	var out_rules []rule
	for _, s_rule := range str_rules {
		out_rules = append(out_rules, parse_rule(s_rule))
	}
	return out_rules
}

func ways_to_carry(rules *[]rule, my_bag bag) []bag {
	// dcs = direct carriers
	var dcs []bag
	for _, cur_rule := range *rules {
		for key_bag := range cur_rule.carries {
			if key_bag == my_bag {
				dcs = append(dcs, cur_rule.carrier)
			}
		}
	}

	carriers := dcs
	for _, dc := range dcs {
		carriers = append(carriers, ways_to_carry(rules, dc)...)
	}

	return carriers
}

func n_nested_bags(rules *[]rule, my_bag bag) int {
	count := 0
	for _, cur_rule := range *rules {
		if cur_rule.carrier == my_bag {
			for key_bag, val_n := range cur_rule.carries {
				count += val_n + val_n*n_nested_bags(rules, key_bag)
			}
		}
	}

	return count
}

func main() {
	fp, err := os.Open("input_Day7.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	str_rules, err := ReadRules(reader)
	if err != nil {
		fmt.Println(err)
		return
	}
	rules := parse_rules(str_rules)

	var my_bag bag
	my_bag.tone = "shiny"
	my_bag.color = "gold"
	carriers := ways_to_carry(&rules, my_bag)

	var str_carriers []string
	for _, cb := range carriers {
		str_carriers = append(str_carriers, cb.tone+cb.color)
	}
	sort.Strings(str_carriers)
	unique.Strings(&str_carriers)

	fmt.Println(len(str_carriers))

	count := n_nested_bags(&rules, my_bag)
	fmt.Println(count)

}
