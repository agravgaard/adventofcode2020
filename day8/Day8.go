package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func ReadInstructions(r io.Reader) ([]string, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	var output []string
	for scanner.Scan() {
		s_map := scanner.Text()
		output = append(output, s_map)
	}
	return output, scanner.Err()
}

type instruction struct {
	asm string
	val int
}

func parse_instruction(str_instr string) instruction {
	av := strings.Split(str_instr, " ")
	var instr instruction
	instr.asm = av[0]
	instr.val, _ = strconv.Atoi(av[1])
	return instr
}

func parse_instructions(str_instr []string) []instruction {
	var out_instr []instruction
	for _, s_instr := range str_instr {
		out_instr = append(out_instr, parse_instruction(s_instr))
	}
	return out_instr
}

func exec(instr instruction, acc int, i int) (int, int) {
	switch instr.asm {
	case "acc":
		return acc + instr.val, i + 1
	case "jmp":
		return acc, i + instr.val
	}
	// case "nop":
	return acc, i + 1
}

func run_instructions(instr []instruction) (int, []int) {
	accumulator := 0
	var history []int
	for i := 0; i < len(instr); {
		for _, h := range history {
			if h == i {
				return accumulator, history
			}
		}
		history = append(history, i)
		accumulator, i = exec(instr[i], accumulator, i)
	}
	return accumulator, nil
}

func main() {
	fp, err := os.Open("input_Day8.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	str_instr, err := ReadInstructions(reader)
	if err != nil {
		fmt.Println(err)
		return
	}
	instr := parse_instructions(str_instr)

	accumulator, history := run_instructions(instr)
	fmt.Println(accumulator)

	for _, h := range history {
		if instr[h].asm == "nop" {
			instr[h].asm = "jmp"
			acc, hs := run_instructions(instr)
			if hs == nil {
				fmt.Println(acc)
				return
			}
			instr[h].asm = "nop"
		}
		if instr[h].asm == "jmp" {
			instr[h].asm = "nop"
			acc, hs := run_instructions(instr)
			if hs == nil {
				fmt.Println(acc)
				return
			}
			instr[h].asm = "jmp"
		}
	}

}
