package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func ReadPasswords(r io.Reader) ([][2]string, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)
	var result [][2]string
	for scanner.Scan() {
		x_split := strings.Split(scanner.Text(), ": ")
		result = append(result, [2]string{x_split[0], x_split[1]})
	}
	return result, scanner.Err()
}

func count_valid(vec_map [][2]string) (int, error) {
	sum_valid := 0
	for _, pair := range vec_map {
		crit := pair[0]
		num_letter := strings.Split(crit, " ")
		lo_hi := strings.Split(num_letter[0], "-")

		pswd := pair[1]
		n := strings.Count(pswd, num_letter[1])
		lo, err := strconv.Atoi(lo_hi[0])
		if err != nil {
			return sum_valid, err
		}
		hi, err := strconv.Atoi(lo_hi[1])
		if err != nil {
			return sum_valid, err
		}
		if lo <= n && hi >= n {
			sum_valid++
		}
	}
	return sum_valid, nil
}

func main_p1() {
	fp, err := os.Open("input_Day2.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	vec_map, err := ReadPasswords(reader)
	if err != nil {
		fmt.Println(err)
		return
	}
	count, err := count_valid(vec_map)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(count)

}
