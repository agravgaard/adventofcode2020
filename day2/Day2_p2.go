package main

import (
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

func count_valid_p2(vec_map [][2]string) (int, error) {
	sum_valid := 0
	for _, pair := range vec_map {
		crit := pair[0]
		num_letter := strings.Split(crit, " ")
		lo_hi := strings.Split(num_letter[0], "-")

		pswd := pair[1]
		lo, err := strconv.Atoi(lo_hi[0])
		if err != nil {
			return sum_valid, err
		}
		hi, err := strconv.Atoi(lo_hi[1])
		if err != nil {
			return sum_valid, err
		}
		letter := num_letter[1]
		if len(pswd) >= lo && string(pswd[lo-1]) == letter {
			if len(pswd) < hi || string(pswd[hi-1]) != letter {
				sum_valid++
			}
		} else if len(pswd) >= hi && string(pswd[hi-1]) == letter {
			if string(pswd[lo-1]) != letter {
				sum_valid++
			}
		}
	}
	return sum_valid, nil
}

func main() {
        main_p1()
	fp, err := os.Open("input_Day2.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	vec_map, err := ReadPasswords(reader)
	if err != nil {
		fmt.Println(err)
		return
	}
	count, err := count_valid_p2(vec_map)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(count)

}
