package main

import (
	"bufio"
	"fmt"
	"io"
	"math"
	"os"
	"strconv"
	"strings"
)

func read_instructions(r io.Reader) ([]string, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	var output []string
	for scanner.Scan() {
		s_map := scanner.Text()
		output = append(output, s_map)
	}
	return output, scanner.Err()
}

func get_mask01(instr string) (uint64, uint64) {
	bit_str := strings.Split(instr, " = ")[1]
	mask0, mask1 := uint64(0), uint64(0)
	for _, b := range bit_str {
		switch b {
		case '1':
			mask1++
		case '0':
			mask0++
		}
		mask0 <<= 1
		mask1 <<= 1
	}
	mask0 >>= 1
	mask1 >>= 1
	return mask0, mask1
}

func mask01_val(val int, mask0 uint64, mask1 uint64) uint64 {
	new_val := uint64(val)
	new_val |= mask1
	xor_val := new_val ^ mask0
	new_val = xor_val - (xor_val & mask0)
	return new_val
}

func assign_mem_masked(mem_map *map[int]uint64, instr string, mask0 uint64, mask1 uint64) {
	pos_val := strings.Split(instr, "] = ")
	pos, err := strconv.Atoi(strings.TrimPrefix(pos_val[0], "mem["))
	if err != nil {
		fmt.Println(err, " could not convert pos: ", instr)
	}
	val, err := strconv.Atoi(pos_val[1])
	if err != nil {
		fmt.Println(err, " could not convert value: ", instr)
	}
	(*mem_map)[pos] = mask01_val(val, mask0, mask1)

}

func run_instructions(instructions []string, mem_map *map[int]uint64) {
	mask0, mask1 := uint64(0), uint64(0)
	for _, instr := range instructions {
		if strings.HasPrefix(instr, "mask") {
			mask0, mask1 = get_mask01(instr)
		} else if strings.HasPrefix(instr, "mem") {
			assign_mem_masked(mem_map, instr, mask0, mask1)
		}
	}
}

func sum_memmap(mem_map map[int]uint64) uint64 {
	sum := uint64(0)
	for _, val := range mem_map {
		sum += val
	}
	return sum
}

func sum_memmap_v2(mem_map map[uint64]int) uint64 {
	sum := uint64(0)
	for _, val := range mem_map {
		sum += uint64(val)
	}
	return sum
}

func powInt(x, y int) int {
	return int(math.Pow(float64(x), float64(y)))
}

func mask01_pos(pos int, mask0 uint64, mask1 uint64) []uint64 {
	pos64 := uint64(pos)

	maskx := ^(mask0 | mask1)
	pos64 |= mask1

	var v_maskx []uint64
	for i := 0; i < 36; i++ {
		if (maskx>>i)&0x1 != 0 {
			v_maskx = append(v_maskx, 0x1<<i)
		}
	}

	var pos_out []uint64
	pos_out = append(pos_out, pos64&^maskx) // the all-zero option
	sanity_check := 1
	for _, xmask := range v_maskx {
		tmp_pos_out := make([]uint64, len(pos_out))
		for p, opts := range pos_out {
			tmp_pos_out[p] = opts | xmask
			sanity_check++
		}
		pos_out = append(pos_out, tmp_pos_out...)
	}
	if sanity_check != powInt(2, len(v_maskx)) {
		fmt.Println("Sanity check 1 failed")
	}

	return pos_out
}

func assign_mem_masked_addr(mem_map *map[uint64]int, instr string, mask0 uint64, mask1 uint64) {
	pos_val := strings.Split(instr, "] = ")
	pos, err := strconv.Atoi(strings.TrimPrefix(pos_val[0], "mem["))
	if err != nil {
		fmt.Println(err, " could not convert pos: ", instr)
	}
	val, err := strconv.Atoi(pos_val[1])
	if err != nil {
		fmt.Println(err, " could not convert value: ", instr)
	}
	v_new_pos := mask01_pos(pos, mask0, mask1)
	for _, pos := range v_new_pos {
		(*mem_map)[pos] = val
	}

}

func run_instructions_v2(instructions []string, mem_map *map[uint64]int) {
	mask0, mask1 := uint64(0), uint64(0)
	for _, instr := range instructions {
		if strings.HasPrefix(instr, "mask") {
			mask0, mask1 = get_mask01(instr)
		} else if strings.HasPrefix(instr, "mem") {
			assign_mem_masked_addr(mem_map, instr, mask0, mask1)
		}
	}
}

func main() {
	fp, err := os.Open("input_Day14.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	v_strings, err := read_instructions(reader)
	if err != nil {
		fmt.Println(err)
		return
	}

	mem_map := make(map[int]uint64)
	run_instructions(v_strings, &mem_map)

	sum_mem := sum_memmap(mem_map)
	fmt.Println("Sum mem_map:", sum_mem)

	mem_map_v2 := make(map[uint64]int)
	run_instructions_v2(v_strings, &mem_map_v2)

	sum_mem = sum_memmap_v2(mem_map_v2)
	fmt.Println("Sum mem_map_v2:", sum_mem)

}
