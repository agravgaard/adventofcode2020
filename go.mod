module gitlab.com/agravgaard/adventofcode2020

go 1.15

require (
	github.com/mpvl/unique v0.0.0-20150818121801-cbe035fff7de
	github.com/stretchr/testify v1.6.1
)
