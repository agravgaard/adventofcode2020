package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
)

func Max(x, y uint) uint {
	if x > y {
		return x
	}
	return y
}

func bin_to_id(s_bin string) uint {
	fb_val := uint(0)
	i := 0
	for ; i < 7; i++ {
		if s_bin[i] == 'B' {
			fb_val++
		}
		fb_val = fb_val << 1
	}
	fb_val = fb_val >> 1
	rl_val := uint(0)
	for ; i < 10; i++ {
		if s_bin[i] == 'R' {
			rl_val++
		}
		rl_val = rl_val << 1
	}
	rl_val = rl_val >> 1
	return fb_val*8 + rl_val
}

func ReadSeatIDs(r io.Reader) ([]uint, error) {
	var seat_ids []uint
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	for scanner.Scan() {
		s_bin := scanner.Text()
		seat_ids = append(seat_ids, bin_to_id(s_bin))
	}
	return seat_ids, scanner.Err()
}

func main() {
	fp, err := os.Open("input_Day5.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	seat_ids, err := ReadSeatIDs(reader)
	if err != nil {
		fmt.Println(err)
		return
	}
	max_id := uint(0)
	for _, id := range seat_ids {
		max_id = Max(max_id, id)
	}
	fmt.Println(max_id)

	sort.Slice(seat_ids, func(i, j int) bool { return seat_ids[i] < seat_ids[j] })

	for i := 1; i < len(seat_ids); i++ {
		if seat_ids[i]-seat_ids[i-1] > 1 {
			fmt.Println(seat_ids[i] - 1)
		}
	}

}
