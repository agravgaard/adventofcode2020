package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strconv"
	"strings"
)

type Passport struct {
	byr string // (Birth Year)
	iyr string // (Issue Year)
	eyr string // (Expiration Year)
	hgt string // (Height)
	hcl string // (Hair Color)
	ecl string // (Eye Color)
	pid string // (Passport ID)
	cid string // (Country ID)
}

func is_n_digit_within(val string, n uint, low int, high int) bool {
	if uint(len(val)) != n {
		return false
	}
	i_val, err := strconv.Atoi(val)
	if err != nil {
		return false
	}
	if i_val >= low && i_val <= high {
		return true
	}
	return false
}

// byr (Birth Year) - four digits; at least 1920 and at most 2002.
func is_byr_valid(val string) bool {
	return is_n_digit_within(val, 4, 1920, 2002)
}

// iyr (Issue Year) - four digits; at least 2010 and at most 2020.
func is_iyr_valid(val string) bool {
	return is_n_digit_within(val, 4, 2010, 2020)
}

// eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
func is_eyr_valid(val string) bool {
	return is_n_digit_within(val, 4, 2020, 2030)
}

// hgt (Height) - a number followed by either cm or in:
func is_hgt_valid(val string) bool {
	// If cm, the number must be at least 150 and at most 193.
	if strings.HasSuffix(val, "cm") {
		trimmed_val := strings.TrimSuffix(val, "cm")
		return is_n_digit_within(trimmed_val, 3, 150, 193)
	}
	// If in, the number must be at least 59 and at most 76.
	if strings.HasSuffix(val, "in") {
		trimmed_val := strings.TrimSuffix(val, "in")
		return is_n_digit_within(trimmed_val, 2, 59, 76)
	}
	return false
}

// hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
func is_hcl_valid(val string) bool {
	if len(val) != 7 || !strings.HasPrefix(val, "#") {
		return false
	}

	test_str := "0123456789abcdef"
	for i := 1; i < len(val); i++ {
		if !strings.Contains(test_str, string(val[i])) {
			return false
		}
	}
	return true
}

// ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
func is_ecl_valid(val string) bool {
	switch val {
	case "amb":
		return true
	case "blu":
		return true
	case "brn":
		return true
	case "gry":
		return true
	case "grn":
		return true
	case "hzl":
		return true
	case "oth":
		return true
	}
	return false
}

// pid (Passport ID) - a nine-digit number, including leading zeroes.
func is_pid_valid(val string) bool {
	if len(val) != 9 {
		return false
	}
	_, err := strconv.Atoi(val)
	return err == nil
}

// cid (Country ID) - ignored, missing or not.

func passport_valid(p Passport) bool {
	return is_byr_valid(p.byr) &&
		is_iyr_valid(p.iyr) &&
		is_eyr_valid(p.eyr) &&
		is_hgt_valid(p.hgt) &&
		is_hcl_valid(p.hcl) &&
		is_ecl_valid(p.ecl) &&
		is_pid_valid(p.pid)
}

func ReadPassports(r io.Reader) (int, error) {
	n_valid_passports := 0
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	curr_pass := Passport{}
	for scanner.Scan() {
		s_map := scanner.Text()
		if s_map == "" {
			if passport_valid(curr_pass) {
				n_valid_passports++
			}
			curr_pass = Passport{}
			continue
		}
		fields := strings.Split(s_map, " ")
		for _, field := range fields {
			field_kv := strings.Split(field, ":")
			switch field_kv[0] {
			case "byr": // (Birth Year)
				curr_pass.byr = field_kv[1]
			case "iyr": // (Issue Year)
				curr_pass.iyr = field_kv[1]
			case "eyr": // (Expiration Year)
				curr_pass.eyr = field_kv[1]
			case "hgt": // (Height)
				curr_pass.hgt = field_kv[1]
			case "hcl": // (Hair Color)
				curr_pass.hcl = field_kv[1]
			case "ecl": // (Eye Color)
				curr_pass.ecl = field_kv[1]
			case "pid": // (Passport ID)
				curr_pass.pid = field_kv[1]
			case "cid": // (Country ID)
				curr_pass.cid = field_kv[1]
			}
		}
	}
	return n_valid_passports, scanner.Err()
}

func main() {
	fp, err := os.Open("input_Day4.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	n_valid_passports, err := ReadPassports(reader)
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(n_valid_passports)

}
