package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func ReadSeats(r io.Reader) ([]string, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	var output []string
	for scanner.Scan() {
		s_map := scanner.Text()
		output = append(output, s_map)
	}
	return output, scanner.Err()
}

const (
	F = iota // Floor
	L        // Free
	O        // Occupied
)

func rune_to_seat(b rune) int {
	if string(b) == "L" {
		return L
	}
	return F
}

func string_to_seats(str string) []int {
	var output []int
	for _, b := range str {
		output = append(output, rune_to_seat(b))
	}
	return output
}

func strings_to_seat_array(v_str []string) [][]int {
	var output [][]int
	for _, str := range v_str {
		output = append(output, string_to_seats(str))
	}
	return output
}

func neigbourhood_occupy(sa [3][3]int) (int, int) {
	if sa[1][1] == F {
		// floor don't change
		return F, 0
	}
	if sa[1][1] == L {
		for i := 0; i < 3; i++ {
			for j := 0; j < 3; j++ {
				if sa[i][j] == O {
					return L, 0
				}
			}
		}
		// Occupy only if all seats around is empty
		return O, 1
	}
	// else O(ccupied)
	// count occupied neghbours (not itself so start at -1)
	occ_nb := -1
	for i := 0; i < 3; i++ {
		for j := 0; j < 3; j++ {
			if sa[i][j] == O {
				occ_nb++
			}
		}
	}
	if occ_nb >= 4 {
		return L, 1
	}
	return O, 0
}

func clamp_pm1(i int, size int) (int, int) {
	xminoff := 0
	xmaxoff := 0
	if i-1 < 0 {
		xminoff = 1
	}
	if i+1 >= size {
		xmaxoff = -1
	}
	return xminoff, xmaxoff
}

func iclamp_pm1(i int, size int, arr *[][]int) [3][]int {
	xminoff, xmaxoff := clamp_pm1(i, size)

	var output [3][]int
	for k := 0; k < 3; k++ {
		output[k] = make([]int, len((*arr)[i]))
	}

	for x := 0 + xminoff; x < 3+xmaxoff; x++ {
		output[x] = (*arr)[i+x-1]
	}

	return output
}

func jclamp_pm1(j int, size int, arr *[3][]int) [3][3]int {
	xminoff, xmaxoff := clamp_pm1(j, size)

	var output [3][3]int
	for k := 0; k < 3; k++ {
		for l := 0; l < 3; l++ {
			output[k][l] = 0
		}
	}

	for y := 0; y < 3; y++ {
		for x := 0 + xminoff; x < 3+xmaxoff; x++ {
			output[y][x] = (*arr)[y][j+x-1]
		}
	}

	return output
}

func occupy_iteration(sa [][]int) (int, [][]int) {
	out_sa := make([][]int, len(sa))
	n_changes := 0
	changed := 0
	for i, sv := range sa {
		out_sa[i] = make([]int, len(sv))
		arr3xN := iclamp_pm1(i, len(sa), &sa)
		for j := range sv {
			arr3x3 := jclamp_pm1(j, len(sv), &arr3xN)
			out_sa[i][j], changed = neigbourhood_occupy(arr3x3)
			n_changes += changed
		}
	}

	return n_changes, out_sa
}

func occupy_seats(sa *[][]int) {
	n_changes := 0
	for {
		n_changes, *sa = occupy_iteration(*sa)
		if n_changes == 0 {
			break
		}
	}
}

func count_occupied(sa [][]int) uint64 {
	count := uint64(0)
	for _, sv := range sa {
		for _, s := range sv {
			if s == O {
				count++
			}
		}
	}
	return count
}

func main1() {
	fp, err := os.Open("input_Day11.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	v_strings, err := ReadSeats(reader)
	if err != nil {
		fmt.Println(err)
		return
	}

	sa := strings_to_seat_array(v_strings)

	occupy_seats(&sa)

	fmt.Println(count_occupied(sa))

}
