package main

import (
	"fmt"
	"io"
	"os"
)

func laser(i, j, x, y int, sa *[][]int) int {
	xsize := len(*sa)
	ysize := len((*sa)[0])
	for stepper := 1; ; stepper++ {
		ix := i + stepper*x
		jy := j + stepper*y
		if ix < 0 || ix >= xsize || jy < 0 || jy >= ysize {
			return F
		}
		if (*sa)[ix][jy] != F {
			return (*sa)[ix][jy]
		}
	}
}

func count_occupied_los(i, j int, sa *[][]int) int {
	count := 0
	for x := -1; x <= 1; x++ {
		for y := -1; y <= 1; y++ {
			if x == 0 && y == 0 {
				continue
			}
			s := laser(i, j, x, y, sa)
			if s == O {
				count++
			}
		}
	}
	return count
}

func los_rule_L(i, j int, sa *[][]int) (int, int) {
	// no occupied in los => L->O
	if count_occupied_los(i, j, sa) == 0 {
		return O, 1
	}
	return L, 0
}

func los_rule_O(i, j int, sa *[][]int) (int, int) {
	// five or more occupied => O->L
	if count_occupied_los(i, j, sa) >= 5 {
		return L, 1
	}
	return O, 0
}

func los_occupy(i, j int, sa *[][]int) (int, int) {
	cs := (*sa)[i][j]
	switch cs {
	case F:
		return F, 0
	case L:
		return los_rule_L(i, j, sa)
	}
	// case O:
	return los_rule_O(i, j, sa)
}

func occupy_iteration_los(sa *[][]int) (int, [][]int) {
	out_sa := make([][]int, len(*sa))
	n_changes := 0
	changed := 0
	for i, sv := range *sa {
		out_sa[i] = make([]int, len(sv))
		for j := range sv {
			out_sa[i][j], changed = los_occupy(i, j, sa)
			n_changes += changed
		}
	}

	return n_changes, out_sa
}

func occupy_seats_los(sa *[][]int) {
	n_changes := 0
	for {
		n_changes, *sa = occupy_iteration_los(sa)
		if n_changes == 0 {
			break
		}
	}
}

func main() {
        main1()
	fp, err := os.Open("input_Day11.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	v_strings, err := ReadSeats(reader)
	if err != nil {
		fmt.Println(err)
		return
	}

	sa := strings_to_seat_array(v_strings)

	occupy_seats_los(&sa)

	fmt.Println(count_occupied(sa))

}
