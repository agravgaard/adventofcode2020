package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strconv"
	"strings"
)

func read_rules_and_tickets(r io.Reader) ([]string, []string, []string, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanLines)

	var rules []string
	var my_ticket []string
	var tickets []string
	reading_part := 0
	for scanner.Scan() {
		s_map := scanner.Text()
		if s_map == "" {
			reading_part++
			continue
		}
		switch reading_part {
		case 0:
			rules = append(rules, s_map)
		case 1:
			my_ticket = append(my_ticket, s_map)
		case 2:
			tickets = append(tickets, s_map)
		}
	}
	return rules, my_ticket, tickets, scanner.Err()
}

type rule struct {
	name string
	lo1  int
	hi1  int
	lo2  int
	hi2  int
}

func make_rule(s_rule string) rule {
	var out rule
	name_vals := strings.Split(s_rule, ": ")
	out.name = name_vals[0]
	lohi1_lohi2 := strings.Split(name_vals[1], " or ")
	lohi1 := strings.Split(lohi1_lohi2[0], "-")
	lohi2 := strings.Split(lohi1_lohi2[1], "-")
	out.lo1, _ = strconv.Atoi(lohi1[0])
	out.hi1, _ = strconv.Atoi(lohi1[1])
	out.lo2, _ = strconv.Atoi(lohi2[0])
	out.hi2, _ = strconv.Atoi(lohi2[1])
	return out
}

func interpret_rules(s_rules []string) []rule {
	rules := make([]rule, len(s_rules))
	for i, s_rule := range s_rules {
		rules[i] = make_rule(s_rule)
	}
	return rules
}

func make_ticket(s_ticket string) []int {
	v_ticket := strings.Split(s_ticket, ",")
	out := make([]int, len(v_ticket))
	for i, sint := range v_ticket {
		out[i], _ = strconv.Atoi(sint)
	}
	return out
}

func interpret_tickets(s_tickets []string) [][]int {
	tickets := make([][]int, len(s_tickets))
	for i, s_ticket := range s_tickets {
		tickets[i] = make_ticket(s_ticket)
	}
	return tickets
}

func in_rule(val int, r rule) bool {
	if val >= r.lo1 && val <= r.hi1 {
		return true
	} else if val >= r.lo2 && val <= r.hi2 {
		return true
	}
	return false
}

func in_rules(val int, rules *[]rule) bool {
	for _, r := range *rules {
		if in_rule(val, r) {
			return true
		}
	}
	return false
}

func get_invalid_numbers(ticket []int, rules *[]rule) []int {
	var out []int
	for _, val := range ticket {
		if !in_rules(val, rules) {
			out = append(out, val)
		}
	}
	return out
}

func get_all_invalid_numbers(tickets [][]int, rules []rule) []int {
	var invalids []int

	for _, ticket := range tickets {
		new_invalids := get_invalid_numbers(ticket, &rules)
		invalids = append(invalids, new_invalids...)
	}

	return invalids
}

func sum_ints(ints []int) int64 {
	sum := int64(0)
	for _, val := range ints {
		sum += int64(val)
	}
	return sum
}

func remove_idx(slice []rule, s int) []rule {
	return append(slice[:s], slice[s+1:]...)
}

func remove_indicies(slice []rule, s []int) []rule {
	sort.Ints(s)
	for i := len(s) - 1; i >= 0; i-- {
		slice = remove_idx(slice, s[i])
	}
	return slice
}

func make_candidates(size int, rules []rule) [][]rule {
	field_candidates := make([][]rule, size)
	for i := range field_candidates {
		field_candidates[i] = make([]rule, len(rules))
		for j, r := range rules {
			field_candidates[i][j] = r
		}
	}
	return field_candidates
}

func discard_invalid_rules(field_candidates *[][]rule, tickets [][]int, rules []rule) {
	for _, ticket := range tickets {
		if len(get_invalid_numbers(ticket, &rules)) == 0 {
			for i, candidates := range *field_candidates {
				tik_val := ticket[i]
				var cands_to_pop []int
				for j, r := range candidates {
					if !in_rule(tik_val, r) {
						cands_to_pop = append(cands_to_pop, j)
					}
				}
				(*field_candidates)[i] = remove_indicies(candidates, cands_to_pop)
				// popping time
			}
		}
	}
}

func sum_len(rvecvec [][]rule) int {
	sum := 0
	for _, rvec := range rvecvec {
		sum += len(rvec)
	}
	return sum
}

func remove_rule_by_name(field_candidates *[][]rule, name string) {
	for ifc, candidates := range *field_candidates {
		if len(candidates) > 1 {
			i_to_rm := 69
			for i, r := range candidates {
				if r.name == name {
					i_to_rm = i
				}
			}
			if i_to_rm != 69 {
				(*field_candidates)[ifc] = remove_idx(candidates, i_to_rm)
			}
		}
	}
}

func discard_uniques(field_candidates *[][]rule, not_in []string) string {
	unique_name := ""

	for _, candidates := range *field_candidates {

		if len(candidates) == 1 {
			unique_name = candidates[0].name
			for _, skip_name := range not_in {
				if unique_name == skip_name {
					unique_name = ""
				}
			}
		}
		if unique_name != "" {
			break
		}
	}
	if unique_name != "" {
		remove_rule_by_name(field_candidates, unique_name)
	}

	return unique_name
}

func deduce_fields(tickets [][]int, rules []rule) [][]rule {
	field_candidates := make_candidates(len(tickets[1]), rules)
	discard_invalid_rules(&field_candidates, tickets, rules)
	sumlen := sum_len(field_candidates)
	var uniques []string
	for sumlen > len(field_candidates) {
		fmt.Println(sumlen-len(field_candidates), " remaining...")
		unique_name := discard_uniques(&field_candidates, uniques)
		if unique_name != "" {
			uniques = append(uniques, unique_name)
		} else {
			fmt.Println("No new uniques found!")
			break
		}
		sumlen = sum_len(field_candidates)
	}
	return field_candidates
}

func main() {
	fp, err := os.Open("input_Day16.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	reader := io.Reader(fp)
	s_rules, s_my_ticket, s_tickets, err := read_rules_and_tickets(reader)
	if err != nil {
		fmt.Println(err)
		return
	}

	rules := interpret_rules(s_rules)
	tickets := interpret_tickets(s_tickets[1:])
	my_ticket := interpret_tickets(s_my_ticket[1:])
	fmt.Println("My ticket:", my_ticket)

	invalids := get_all_invalid_numbers(tickets, rules)
	sum := sum_ints(invalids)
	fmt.Println("Sum invalid fields:", sum)

	tickets = append(tickets, my_ticket[0])
	field_candidates := deduce_fields(tickets, rules)

	product_departure := 1
	for i, field := range my_ticket[0] {
		var cands []string
		for _, r := range field_candidates[i] {
			cands = append(cands, r.name)
		}
		fmt.Println(field, ":", cands)
		if strings.Contains(cands[0], "departure") {
			product_departure *= field
		}
	}
	fmt.Println("Product departure", product_departure)

}
